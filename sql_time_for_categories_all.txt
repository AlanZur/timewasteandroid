SELECT sum( h.stop - h.start ),
       c.name
  FROM history h
       LEFT JOIN categories c
              ON h.cat_id = c._id
 GROUP BY h.cat_id;

