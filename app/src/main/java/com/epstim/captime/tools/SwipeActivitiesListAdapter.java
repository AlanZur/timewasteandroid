package com.epstim.captime.tools;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.epstim.captime.activities.MainActivity;
import com.epstim.captime.R;
import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.dbmodel.History;
import com.epstim.captime.jsonmodel.JsonHistory;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;

import org.apache.http.HttpResponse;

import java.util.List;

/**
 *
 * Created by Alan Żur on 2014-09-05.
 */
public class SwipeActivitiesListAdapter extends ArrayAdapter<JsonHistory> {
    List<JsonHistory> items;
    Context context;
    Activity activity;
    int resId;
    SparseArray<String> categories;
    Fragment fragment;

    public SwipeActivitiesListAdapter(Activity context, Fragment fragment, int resource, List<JsonHistory> objects) {
        super(context, resource, objects);
        items = objects;
        this.context = context;
        this.activity = context;
        resId = resource;
        this.fragment = fragment;
        categories = ((MainActivity)context).getCategories();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ActivityHolder holder = null;
        if(row == null){
            LayoutInflater inflater = LayoutInflater.from(context);
            row = inflater.inflate(resId, parent, false);

            holder = new ActivityHolder();

            holder.delete = (ImageButton) row.findViewById(R.id.deleteButton);
            holder.share = (ImageButton) row.findViewById(R.id.shareButton);
            holder.icon = (ImageView) row.findViewById(R.id.categoryIcon);
            holder.name = (TextView) row.findViewById(R.id.categoryName);
            holder.front = (LinearLayout) row.findViewById(R.id.front);
            holder.back = (LinearLayout) row.findViewById(R.id.back);
            row.setTag(holder);
        }else{
            holder = (ActivityHolder) row.getTag();
        }

        final JsonHistory item = items.get(position);
        String categoryName = "";
        Cursor cursor = context.getContentResolver().query(
                Uri.parse(Categories.URI + "/" + item.categoryId),
                Categories.getProjection(),
                Categories.C_ID + " = ?",
                new String[]{String.valueOf(item.categoryId)},
                null
        );
        if(cursor.moveToFirst()){
            categoryName = cursor.getString(cursor.getColumnIndex(Categories.C_NAME));
        }
        cursor.close();
        item.slug = categoryName.toLowerCase().replace(" ", "-");

        holder.front.setBackgroundColor(
                context.getResources().getColor(
                        context.getResources().getIdentifier(
                                "color_" + categoryName.toLowerCase().replace(" ", "_"),
                                "color",
                                context.getPackageName())));

        holder.icon.setImageResource(
                context.getResources().getIdentifier(
                        "icon_" + categoryName.toLowerCase().replace(" ", "_"),
                        "drawable",
                        context.getPackageName()));

        holder.name.setText(categoryName + " " + Tools.formatTime(item.stop - item.start));

        int height = (int) ((parent.getWidth()/3)*0.8);
        int width = parent.getWidth()/3;
        Log.d("ROW", "Width: " + width + " Height: " + height);
        holder.front.getLayoutParams().height = height;
        holder.back.getLayoutParams().height = height;

        final String finalCategoryName = categoryName;

        Tools.TimeContainer container = Tools.getTime(item.stop - item.start);

        final Bundle params = new Bundle();
        params.putString("og:type", "cap-time:" + item.slug.replace("-", "_"));
        params.putString("og:url", "http://timewaste.epst.im");
        params.putString("og:title", "Capture every moment! - Cap time!");
        params.putString("og:description", "Track and check how much time you spend on particular activities, see what distracts you most and improve your productivity!");
        params.putInt("cap-time:hours", container.getHours());
        params.putInt("cap-time:minutes", container.getMinutes());
        params.putInt("cap-time:seconds", container.getSeconds());

        StringBuilder sb = new StringBuilder();
        sb.append("{")
                .append("\"").append("og:type").append("\":\"").append("cap-time:" + item.slug.replace("-", "_")).append("\",")
                .append("\"").append("og:url").append("\":\"").append("http://timewaste.epst.im").append("\",")
                .append("\"").append("og:title").append("\":\"").append("Capture every moment! - Cap time!").append("\",")
                .append("\"").append("og:image").append("\":\"").append("http://timewaste.epst.im/assets/gfx/fb/").append(item.slug).append(".png").append("\",")
                .append("\"").append("og:description").append("\":\"").append("Track and check how much time you spend on particular activities, see what distracts you most and improve your productivity!").append("\",")
                .append("\"").append("cap-time:hours").append("\":\"").append(container.getHours()).append("\",")
                .append("\"").append("cap-time:minutes").append("\":\"").append(container.getMinutes()).append("\",")
                .append("\"").append("cap-time:seconds").append("\":\"").append(container.getSeconds()).append("\"")
        .append("}");

        final Bundle object = new Bundle();
        object.putString(item.slug.replace("-","_"), sb.toString());
        Log.d("OBJECT: ", sb.toString());
        object.putBoolean("fb:explicitly_shared", true);

        final Request request = new Request(
                Session.getActiveSession(),
                "me/cap-time:spent",
                object,
                HttpMethod.POST,
                new Request.Callback() {
                    @Override
                    public void onCompleted(Response response) {
                        if(response != null && response.getError() != null) Log.d("Facebook Share", "Error message:" + response.getError().getErrorCode() + ", " + response.getError().getErrorMessage());
                        if(response != null && response.getGraphObject() != null){
                            Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );

        final int itemId = item.id;

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                request.executeAsync();
                //MainActivity.uiLifecycleHelper.trackPendingDialogCall(shareDialog.present());
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public synchronized void onClick(View v) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                                HttpResponse response = JsonHelper.sendDELETE(JsonHelper.HOST + "records/id/" + itemId);
                                Log.d("DELETE", "host: " + JsonHelper.HOST + "records/id/" + itemId);
                                Log.d("DELETE", "Result code: " + response.getStatusLine().getStatusCode() + " result message: " + response.getStatusLine().getReasonPhrase());
                                if (response.getStatusLine().getStatusCode() == 204) {
                                    items.remove(position);
                                    int id = context.getContentResolver().delete(Uri.parse(History.URI + "/" + itemId), History.C_ID + " = ?", new String[]{String.valueOf(itemId)});
                                    Log.d("DELETE", "database: " + id);
                                    MainActivity.application.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            notifyDataSetChanged();
                                            Toast.makeText(context, "Success!", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                    }).start();
                }
        });
        return row;
    }

    private static class ActivityHolder{
        ImageButton delete;
        ImageButton share;
        ImageView icon;
        TextView name;
        LinearLayout front;
        LinearLayout back;
    }
}
