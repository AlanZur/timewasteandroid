package com.epstim.captime.tools;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.epstim.captime.activities.MainActivity;
import com.epstim.captime.R;

import java.util.List;

/**
 * Created by Alan Żur on 2014-07-24.
 */
public class Tools extends Application{

    private static final String TAG = Tools.class.getSimpleName();

    public static String formatTime(Long milis) {
        long seconds = milis / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;

        return String.format(MainActivity.getAppContext().getString(R.string.time_format_string)
                , hours
                , minutes % 60
                , seconds % 60);
    }

    public static TimeContainer getTime(Long milis) {
        long seconds = milis / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;

        return new TimeContainer(hours, minutes % 60, seconds % 60);
    }

    public static boolean isServiceRunning(Context context, String serviceClassName) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);

        for (ActivityManager.RunningServiceInfo runningServiceInfo : services) {
            if (runningServiceInfo.service.getClassName().equals(serviceClassName)) {
                Log.d(TAG, "Service is running");
                return true;
            }
        }
        Log.d(TAG, "Service is not running");
        return false;
    }

    public static boolean isValidEmail(CharSequence email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public static class TimeContainer{
        private int hours;
        private int minutes;
        private int seconds;

        public TimeContainer(int hours, int minutes, int seconds){
            this.setHours(hours);
            this.setMinutes(minutes);
            this.setSeconds(seconds);
        }

        public TimeContainer(long hours, long minutes, long seconds){
            this.setHours((int) hours);
            this.setMinutes((int) minutes);
            this.setSeconds((int) seconds);
        }

        public int getHours() {
            return hours;
        }

        public void setHours(int hours) {
            this.hours = hours;
        }

        public int getMinutes() {
            return minutes;
        }

        public void setMinutes(int minutes) {
            this.minutes = minutes;
        }

        public int getSeconds() {
            return seconds;
        }

        public void setSeconds(int seconds) {
            this.seconds = seconds;
        }
    }
}
