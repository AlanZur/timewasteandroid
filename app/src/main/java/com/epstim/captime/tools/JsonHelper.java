package com.epstim.captime.tools;

import android.net.Uri;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Alan Żur on 2014-08-04.
 */
public class JsonHelper {
    //public static final String HOST = "http://api.cap-time.com/";
    public static final String HOST = "http://apitimewaste.epst.im/";
    public static final String USER_NAME = "android";
    public static final String PASSWORD = "MbM)]5FW2LgJ-+m";

    public static String sendPOST(String url, String jsonBody) {
        HttpClient client = new DefaultHttpClient();
        Uri uri = Uri.parse(url);
        AuthScope authScope = new AuthScope(uri.getHost(), uri.getPort());
        UsernamePasswordCredentials usernamePasswordCredentials = new UsernamePasswordCredentials(USER_NAME, PASSWORD);
        ((AbstractHttpClient) client).getCredentialsProvider().setCredentials(authScope, usernamePasswordCredentials);
        BasicHttpContext localHttpContext = new BasicHttpContext();
        BasicScheme basicScheme = new BasicScheme();
        localHttpContext.setAttribute("preemptive-auth", basicScheme);
        HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
        HttpPost post = new HttpPost(url);
        StringEntity entity;
        try {
            entity = new StringEntity(jsonBody, "UTF-8");
            post.setHeader("Content-Type", "application/json");
            post.setHeader("Accept", "application/json");
            post.setEntity(entity);
            HttpResponse response = client.execute(host, post);

            StringBuilder stringBuilder = new StringBuilder();
            if(response != null) {
                //if (response.getStatusLine().getStatusCode() >= 400)
                //    return String.valueOf(response.getStatusLine().getStatusCode());
                InputStream inputStream = response.getEntity().getContent();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
            }
            return stringBuilder.toString();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String sendGET(String url) {
        HttpClient client = new DefaultHttpClient();
        Uri uri = Uri.parse(url);
        AuthScope authScope = new AuthScope(uri.getHost(), uri.getPort());
        UsernamePasswordCredentials usernamePasswordCredentials = new UsernamePasswordCredentials(USER_NAME, PASSWORD);
        ((AbstractHttpClient) client).getCredentialsProvider().setCredentials(authScope, usernamePasswordCredentials);
        BasicHttpContext localHttpContext = new BasicHttpContext();
        BasicScheme basicScheme = new BasicScheme();
        localHttpContext.setAttribute("preemptive-auth", basicScheme);
        HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
        HttpGet get = new HttpGet(url);
        try {
            get.setHeader("Accept", "application/json");
            HttpResponse response = client.execute(host, get);

            StringBuilder stringBuilder = new StringBuilder();
            if(response != null) {
                //if (response.getStatusLine().getStatusCode() >= 400)
                //    return String.valueOf(response.getStatusLine().getStatusCode());
                InputStream inputStream = response.getEntity().getContent();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
            }
            return stringBuilder.toString();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String sendPUT(String url, String jsonBody) {
        HttpClient client = new DefaultHttpClient();
        Uri uri = Uri.parse(url);
        AuthScope authScope = new AuthScope(uri.getHost(), uri.getPort());
        UsernamePasswordCredentials usernamePasswordCredentials = new UsernamePasswordCredentials(USER_NAME, PASSWORD);
        ((AbstractHttpClient) client).getCredentialsProvider().setCredentials(authScope, usernamePasswordCredentials);
        BasicHttpContext localHttpContext = new BasicHttpContext();
        BasicScheme basicScheme = new BasicScheme();
        localHttpContext.setAttribute("preemptive-auth", basicScheme);
        HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
        HttpPut put = new HttpPut(url);
        StringEntity entity;
        try {
            entity = new StringEntity(jsonBody, "UTF-8");
            put.setHeader("Content-Type", "application/json");
            put.setHeader("Accept", "application/json");
            put.setEntity(entity);
            HttpResponse response = client.execute(host, put);

            StringBuilder stringBuilder = new StringBuilder();
            if(response != null) {
                //if (response.getStatusLine().getStatusCode() >= 400)
                //    return String.valueOf(response.getStatusLine().getStatusCode());
                InputStream inputStream = response.getEntity().getContent();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
            }
            return stringBuilder.toString();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static HttpResponse sendDELETE(String url) {
        HttpClient client = new DefaultHttpClient();
        Uri uri = Uri.parse(url);
        AuthScope authScope = new AuthScope(uri.getHost(), uri.getPort());
        UsernamePasswordCredentials usernamePasswordCredentials = new UsernamePasswordCredentials(USER_NAME, PASSWORD);
        ((AbstractHttpClient) client).getCredentialsProvider().setCredentials(authScope, usernamePasswordCredentials);
        BasicHttpContext localHttpContext = new BasicHttpContext();
        BasicScheme basicScheme = new BasicScheme();
        localHttpContext.setAttribute("preemptive-auth", basicScheme);
        HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
        HttpDelete put = new HttpDelete(url);
        try {
            put.setHeader("Content-Type", "application/json");
            put.setHeader("Accept", "application/json");
            HttpResponse response = client.execute(host, put);

            if(response != null){
                return response;
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String sendImage(String url, String filePath) {
        try {
            Uri uri = Uri.parse(url);
            File file = new File(filePath);
            //Limit wielkości 2MB
            int fileSize = (int) (file.length() / 1024);
            if (fileSize < 2048 && fileSize > 0) {
                HttpClient httpClient = new DefaultHttpClient();
                AuthScope authScope = new AuthScope(uri.getHost(), uri.getPort());
                UsernamePasswordCredentials usernamePasswordCredentials = new UsernamePasswordCredentials(USER_NAME, PASSWORD);
                ((AbstractHttpClient) httpClient).getCredentialsProvider().setCredentials(authScope, usernamePasswordCredentials);
                BasicHttpContext localHttpContext = new BasicHttpContext();
                BasicScheme basicScheme = new BasicScheme();
                localHttpContext.setAttribute("preemptive-auth", basicScheme);

                HttpHost targetHttpHost = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("enctype", "multipart/form-data");

                MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
                multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                multipartEntity.addPart("userfile", new FileBody(file));
                httpPost.setEntity(multipartEntity.build());

                HttpResponse httpResponse = httpClient.execute(targetHttpHost, httpPost, localHttpContext);

                int statusCode = httpResponse.getStatusLine().getStatusCode();
                String reason = httpResponse.getStatusLine().getReasonPhrase();

                return "Status code: " + statusCode + " " + reason;
            } else {
                return "Please select image smaller than 2MB";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
