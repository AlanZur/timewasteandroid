package com.epstim.captime.tools;

import android.location.Location;

/**
 * Created by Alan Żur on 2014-08-01.
 */
public interface LocationTracker {
    public void start();

    public void start(LocationUpdateListener update);

    public void stop();

    public boolean hasLocation();

    public boolean hasPossiblyStaleLocation();

    public Location getLocation();

    public Location getPossiblyStaleLocation();

    public interface LocationUpdateListener {
        public void onUpdate(Location oldLoc, long oldTime, Location newLoc, long newTime);
    }

}
