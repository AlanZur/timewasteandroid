package com.epstim.captime.tools;

import android.content.SharedPreferences;

import com.epstim.captime.jsonmodel.JsonUser;

/**
 * Created by Alan Żur on 2014-07-23.
 */
public interface ApplicationInterface {
    public void showLoginFragment();

    public void showDashboardFragment();

    public void showStatisticsFragment();

    public void showActivityFragment(String name, int id);

    public void startTimer(String category);

    public void stopTimer(String category);

    public void saveUserData(JsonUser user);

    public void downloadRecords();

    public SharedPreferences getPreferences();

    public void isConnected(Boolean isConnected);

    public void showNoConnectionScreen();

}
