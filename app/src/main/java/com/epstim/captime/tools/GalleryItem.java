package com.epstim.captime.tools;

import android.graphics.Bitmap;

/**
 * Created by Alan Żur on 2014-05-14.
 */
public class GalleryItem {
    private String name;
    private String timer;
    private final int id;
    private Bitmap bmp;
    private boolean active;

    public GalleryItem(String name, Bitmap bmp, int id, String timer) {
        this.id = id;
        this.setBmp(bmp);
        this.setName(name);
        setActive(false);
        setTimer(timer);
    }

    public String getName() {
        return name;
    }

    public Bitmap getBmp() {
        return bmp;
    }

    public void setBmp(Bitmap bmp) {
        this.bmp = bmp;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }
}
