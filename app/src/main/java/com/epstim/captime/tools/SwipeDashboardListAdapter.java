package com.epstim.captime.tools;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.epstim.captime.R;
import com.epstim.captime.activities.MainActivity;
import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.fragments.DashboardFragment;
import com.epstim.captime.fragments.LastActivitiesFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alan Żur on 2014-09-11.
 */
public class SwipeDashboardListAdapter extends ArrayAdapter<GalleryItem> {
    int resourceId;
    DashboardFragment fragment;
    List<GalleryItem> items;
    final List<GalleryItem> orginal;

    public SwipeDashboardListAdapter(Context context, DashboardFragment fragment, int resource, List<GalleryItem> objects) {
        super(context, resource, objects);
        resourceId = resource;
        this.fragment = fragment;
        this.items = objects;
        orginal = new ArrayList<>(objects);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ActivityHolder holder = null;
        if(row == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            row = inflater.inflate(resourceId, parent, false);

            holder = new ActivityHolder();

            holder.delete = (ImageButton) row.findViewById(R.id.deleteButton);
            holder.show = (ImageButton) row.findViewById(R.id.showButton);
            holder.icon = (ImageView) row.findViewById(R.id.categoryIcon);
            holder.name = (TextView) row.findViewById(R.id.categoryName);
            holder.text = (TextView) row.findViewById(R.id.timerText);
            holder.front = (LinearLayout) row.findViewById(R.id.front);
            holder.back = (LinearLayout) row.findViewById(R.id.back);
            row.setTag(holder);
        }else{
            holder = (ActivityHolder) row.getTag();
        }

        int height = (int) ((parent.getWidth()/3)*0.8);
        int width = parent.getWidth()/3;
        holder.front.getLayoutParams().height = height;
        holder.back.getLayoutParams().height = height;
        final int id = items.get(position).getId();
        if(id > 0){
            holder.icon.setImageBitmap(items.get(position).getBmp());
            holder.name.setText(items.get(position).getName());
            holder.front.setBackgroundColor(
                    getContext().getResources().getColor(
                            getContext().getResources().getIdentifier(
                                    "color_" + orginal.get(position).getName().toLowerCase().replace(" ", "_"),
                                    "color",
                                    getContext().getPackageName()
                            )
                    )
            );
            holder.delete.setImageResource(
                    getContext().getResources().getIdentifier(
                            "ic_delete_" + orginal.get(position).getName().toLowerCase().replace(" ", "_"),
                            "drawable",
                            getContext().getPackageName()
                    )
            );

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues values = new ContentValues();
                    String selection = Categories.C_ID + " = ?";
                    String[] selectionArgs = {String.valueOf(items.get(position).getId())};
                    values.put(Categories.C_FAV, 0);
                    getContext().getContentResolver().update(Categories.URI, values, selection, selectionArgs);
                    items.remove(position);
                    fragment.refresh();
                }
            });

            holder.show.setImageResource(
                    getContext().getResources().getIdentifier(
                            "ic_view_" + orginal.get(position).getName().toLowerCase().replace(" ", "_"),
                            "drawable",
                            getContext().getPackageName()
                    )
            );

            holder.show.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity)getContext()).showActivityFragment(items.get(position).getName(), items.get(position).getId());
                }
            });

            if(items.get(position).getTimer() != null){
                holder.name.setVisibility(View.GONE);
                holder.text.setText(items.get(position).getTimer());
                holder.text.setVisibility(View.VISIBLE);
            }else{
                holder.name.setVisibility(View.VISIBLE);
                holder.text.setText("");
                holder.text.setVisibility(View.GONE);
            }
        }else{
            holder.back.getLayoutParams().height = 0;
            holder.icon.setImageBitmap(items.get(position).getBmp());
            holder.name.setText(items.get(position).getName());
            try {
                holder.front.setBackgroundColor(
                        getContext().getResources().getColor(
                                getContext().getResources().getIdentifier(
                                        "color_" + orginal.get(position).getName().toLowerCase().replace(" ", "_"),
                                        "color",
                                        getContext().getPackageName()
                                )
                        )
                );
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

        return row;
    }

    private void setItemActive(String category, String text){
        for(int i = 0 ; i < items.size() ; i++){
            if(items.get(i).getName().equals(category)){
                items.get(i).setTimer(text);
                break;
            }
        }
    }

    public void setItemsActive(List<String> categories, List<String> texts){
        for(int i = 0 ; i < categories.size() ; i++){
            setItemActive(categories.get(i), texts.get(i));
        }
        notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        if(orginal != null) {
            orginal.clear();
        }
        orginal.addAll(items);
        super.notifyDataSetChanged();
    }

    public void setItemsInactive(){
        for(GalleryItem item: items){
            item.setTimer(null);
        }
        notifyDataSetChanged();
    }

    private static class ActivityHolder{
        ImageButton delete;
        ImageButton show;
        ImageView icon;
        TextView name;
        TextView text;
        LinearLayout front;
        LinearLayout back;
    }
}
