package com.epstim.captime.tools;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.epstim.captime.activities.MainActivity;
import com.epstim.captime.R;
import com.epstim.captime.dbmodel.History;
import com.epstim.captime.jsonmodel.JsonHistory;
import com.epstim.captime.jsonmodel.JsonHistoryList;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Alan Żur on 2014-07-24.
 */
public class TimerService extends Service{
    public static final String CLASS_NAME = TimerService.class.getName();
    public static final String TAG = TimerService.class.getSimpleName();
    public static final String ACTION = "com.epstim.captime.TIMERTICK";
    public static final String ACTION_ADD = "com.epstim.captime.ADD";
    public static final int ACTION_ADD_ID = 1;
    public static final String ACTION_DELETE = "com.epstim.captime.DELETE";
    public static final int ACTION_DELETE_ID = 2;
    public static final String ACTION_CLEAR = "com.epstim.captime.CLEAR";
    public static final int ACTION_CLEAR_ID = 3;
    public static final String TIME_ARG = "time";
    public static final String CATEGORY_ARG = "category";
    public static final int SERVICE_ID = 9876;
    public static final String HISTORY_ARG = "history_object";
    public static final String ACTIVITY_LIST_ARG = "activity_list";
    public static final String CATEGORY_ID_ARG = "category_id";
    private NotificationCompat.Builder builder;
    private JsonHistoryList activities;
    private Intent intent;
    private Handler handler;
    private static int activitiesCount;
    private Runnable sendUpdateToUi = new Runnable() {
        @Override
        public void run() {
            sendBroadcast();
            handler.postDelayed(this, 1000);
        }
    };
    private NotificationManager mNotificationManager;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public synchronized void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(ACTION_ADD)){
                JsonHistory item = (JsonHistory) intent.getSerializableExtra(HISTORY_ARG);
                boolean canAdd = true;
                for (JsonHistory activity : activities) {
                    if (activity.categoryId == item.categoryId) canAdd = false;
                }
                if (canAdd){
                    new NetworkAsyncTask().execute(ACTION_ADD_ID, item);
                }
            }else if(action.equals(ACTION_DELETE)){
                int id = intent.getIntExtra(CATEGORY_ID_ARG, -1);
                if(id >= 0){
                    new NetworkAsyncTask().execute(ACTION_DELETE_ID, id);
                }
            }else if(action.equals(ACTION_CLEAR)){
                activities.clear();
            }
        }
    };

    public TimerService() {
        handler = new Handler();
        activities = new JsonHistoryList();
    }

    private void sendBroadcast() {
        intent.putExtra(ACTIVITY_LIST_ARG, activities);
        Intent bIntent = new Intent(this, MainActivity.class);
        bIntent.putExtra(ACTIVITY_LIST_ARG, activities);
        bIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        builder.setContentText(String.format(getString(R.string.notification_bar_message), activitiesCount));
        builder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, bIntent, PendingIntent.FLAG_CANCEL_CURRENT));
        Notification barNotif = builder.build();
        mNotificationManager.notify(SERVICE_ID, barNotif);

        sendBroadcast(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_ADD);
        filter.addAction(ACTION_DELETE);
        filter.addAction(ACTION_CLEAR);
        registerReceiver(receiver, filter);
        intent = new Intent(ACTION);
        handler.removeCallbacks(sendUpdateToUi);
        handler.postDelayed(sendUpdateToUi, 1000);
        activities = new JsonHistoryList();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if(intent != null && intent.hasExtra(TimerService.ACTIVITY_LIST_ARG)){
            ArrayList<JsonHistory> temp = (ArrayList<JsonHistory>) intent.getSerializableExtra(TimerService.ACTIVITY_LIST_ARG);
            activities = new JsonHistoryList();
            activities.addAll(temp);
            activitiesCount = activities.size();
            if(activities == null || activities.size() <= 0) activities = new JsonHistoryList();
        }

        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent bIntent = new Intent(getApplicationContext(), MainActivity.class);
        bIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pbIntent = PendingIntent.getActivity(getApplicationContext(), 0, bIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(String.format(getString(R.string.notification_bar_message), activitiesCount))
                        .setAutoCancel(true)
                        .setOngoing(true)
                        .setContentIntent(pbIntent);
        Notification barNotif = builder.build();
        this.startForeground(SERVICE_ID, barNotif);
        //return START_STICKY;
        //return START_REDELIVER_INTENT;
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        handler.removeCallbacks(sendUpdateToUi);
        stopForeground(true);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class NetworkAsyncTask extends AsyncTask<Object, Void, String>{
        Gson gson;

        public NetworkAsyncTask() {
            super();
            gson = new Gson();
        }

        @Override
        protected synchronized String doInBackground(Object... params) {
            Integer actionId = (Integer) params[0];
            String result = null;
            String jsonBody;
            switch (actionId){
                case ACTION_ADD_ID:
                    JsonHistory history = (JsonHistory) params[1];
                    jsonBody = gson.toJson(history, JsonHistory.class);
                    if (jsonBody != null) {
                        result = JsonHelper.sendPOST(JsonHelper.HOST + "records", jsonBody);
                        if(result != null) {
                            Log.d(TAG, result);
                            JsonHistory toInsert;
                            try {
                                toInsert = gson.fromJson(result, JsonHistory.class);
                            }catch(Exception ex){
                                return null;
                            }
                            toInsert.start *= 1000;
                            ContentValues values = new ContentValues();
                            values.put(History.C_ID, toInsert.id);
                            values.put(History.C_CAT_ID, toInsert.categoryId);
                            values.put(History.C_LAT, toInsert.latitude);
                            values.put(History.C_LONG, toInsert.longitude);
                            values.put(History.C_START, toInsert.start);
                            Uri uri = getContentResolver().insert(History.URI, values);
                            Log.d(TAG, "Inserted uri: " + uri);
                            activities.add(toInsert);
                            activitiesCount++;
                            result = "Success!";
                        }
                    }
                    else result = null;
                    break;
                case  ACTION_DELETE_ID:
                    Integer id = (Integer) params[1];
                    for(int i = 0 ; i < activities.size() ; i++){
                        if(activities.get(i).categoryId == id){
                            JsonHistory object = activities.get(i);
                            object.stop = new Date().getTime();

                            jsonBody = gson.toJson(object, JsonHistory.class);
                            result = JsonHelper.sendPUT(JsonHelper.HOST + "/records/id/" + object.id, jsonBody);
                            if(result != null && result.startsWith("{")){
                                Log.d(TAG, result);
                                activities.remove(i);
                                ContentValues values = new ContentValues();
                                values.put(History.C_STOP, object.stop);
                                String[] selectionArgs = {String.valueOf(object.id)};
                                int updated = getContentResolver().update(History.URI, values, History.C_ID + "=?", selectionArgs);
                                Log.d(TAG, "Updated: " + updated + " records");
                                activitiesCount--;
                                result = "Success!";
                            }
                            else result = null;
                        }
                    }
                    break;
                default:
                    result = null;
                    break;
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if(result != null){
                Toast.makeText(MainActivity.getAppContext(), result, Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(MainActivity.getAppContext(), "Failed!", Toast.LENGTH_SHORT).show();
            }
            //sendBroadcast();
        }
    }
}
