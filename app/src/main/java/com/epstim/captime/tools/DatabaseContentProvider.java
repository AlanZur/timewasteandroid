package com.epstim.captime.tools;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.dbmodel.History;

/**
 * Created by Alan Żur on 2014-07-25.
 */
public class DatabaseContentProvider extends ContentProvider {
    public static final String AUTHORITY = "com.epstim.captime.contentprovider";
    public static final Uri URI = Uri.parse("content://" + AUTHORITY);
    private static final int CATEGORIES = 10;
    private static final int CATEGORY_ID = 11;
    private static final int HISTORY = 20;
    private static final int HISTORY_ID = 21;
    private static final int HISTORY_OVERALL = 22;
    private static final int HISTORY_LIMIT = 23;
    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        URI_MATCHER.addURI(AUTHORITY, Categories.BASE_PATH, CATEGORIES);
        URI_MATCHER.addURI(AUTHORITY, Categories.BASE_PATH + "/#", CATEGORIES);
        URI_MATCHER.addURI(AUTHORITY, History.BASE_PATH, HISTORY);
        URI_MATCHER.addURI(AUTHORITY, History.BASE_PATH + "/#", HISTORY_ID);
        URI_MATCHER.addURI(AUTHORITY, History.BASE_PATH + "/overall", HISTORY_OVERALL);
        URI_MATCHER.addURI(AUTHORITY, History.BASE_PATH + "/limit/#", HISTORY_LIMIT);
    }

    private static final String TAG = DatabaseContentProvider.class.getSimpleName();

    private DbHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new DbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String groupBy = null;
        String limit = null;
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        int uriType = URI_MATCHER.match(uri);
        switch (uriType) {
            case CATEGORIES:
                queryBuilder.setTables(Categories.T_NAME);
                break;
            case CATEGORY_ID:
                queryBuilder.setTables(Categories.T_NAME);
                queryBuilder.appendWhere(Categories.T_NAME + "." + Categories.C_ID + " = " + uri.getLastPathSegment());
                break;
            case HISTORY:
                queryBuilder.setTables(History.T_NAME);
                break;
            case HISTORY_ID:
                queryBuilder.setTables(History.T_NAME);
                queryBuilder.appendWhere(History.T_NAME + "." + History.C_ID + " = " + uri.getLastPathSegment());
                break;
            case HISTORY_OVERALL:
                queryBuilder.setTables(History.T_NAME
                        + " h LEFT JOIN " + Categories.T_NAME + " c on " + "c." + Categories.C_ID + " = h." + History.C_CAT_ID);
                groupBy = "h." + History.C_CAT_ID;
                limit = String.valueOf(5);
                break;
            case HISTORY_LIMIT:
                queryBuilder.setTables(History.T_NAME);
                limit = uri.getLastPathSegment();
                break;
            default:
                throw new IllegalArgumentException("Invalid Uri: " + uri);
        }
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, groupBy, null, sortOrder, limit);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = URI_MATCHER.match(uri);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long id = 0l;
        switch (uriType) {
            case CATEGORIES:
                id = db.insertWithOnConflict(Categories.T_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                break;
            case HISTORY:
                id = db.insertWithOnConflict(History.T_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                break;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(uri + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = URI_MATCHER.match(uri);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int id = 0;
        switch (uriType) {
            case HISTORY:
                id = db.delete(History.T_NAME, null, null);
                break;
            case HISTORY_ID:
                id = db.delete(History.T_NAME, selection, selectionArgs);
                break;
        }
        return id;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = URI_MATCHER.match(uri);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rowsUpdated = 0;
        switch (uriType) {
            case CATEGORIES:
                rowsUpdated = db.updateWithOnConflict(Categories.T_NAME, values, selection, selectionArgs, SQLiteDatabase.CONFLICT_IGNORE);
                break;
            case HISTORY:
                rowsUpdated = db.update(History.T_NAME, values, selection, selectionArgs);
                break;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

}
