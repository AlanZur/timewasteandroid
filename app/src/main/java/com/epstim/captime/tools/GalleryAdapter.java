package com.epstim.captime.tools;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.epstim.captime.R;

import java.util.List;

/**
 * Created by Alan Żur on 2014-05-14.
 */
public class GalleryAdapter extends BaseAdapter {
    private List<GalleryItem> items;
    private LayoutInflater inflater;
    private Context context;

    public GalleryAdapter(Context context, List<GalleryItem> items) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.items = items;
    }

    public void setOtherItemsDisabled(String name) {
        if (items == null) return;
        for (GalleryItem item : items) {
            if (item.getName().equals(name)) {
                item.setActive(true);
                item.setBmp(BitmapFactory.decodeResource(context.getResources(), context.getResources().getIdentifier("icon_" + item.getName().toLowerCase().replace(" ", "_") + "_active", "drawable", context.getPackageName())));
            } else {
                item.setActive(false);
                item.setBmp(BitmapFactory.decodeResource(context.getResources(), context.getResources().getIdentifier("icon_" + item.getName().toLowerCase().replace(" ", "_"), "drawable", context.getPackageName())));
            }
        }
        notifyDataSetChanged();
    }

    public void clearInactive(){
        for(GalleryItem item : items){
            if(item.isActive()) item.setBmp(BitmapFactory.decodeResource(context.getResources(), context.getResources().getIdentifier("icon_" + item.getName().toLowerCase().replace(" ", "_").replace(" ", "_") + "_active", "drawable", context.getPackageName())));
            else item.setBmp(BitmapFactory.decodeResource(context.getResources(), context.getResources().getIdentifier("icon_" + item.getName().toLowerCase().replace(" ", "_").replace(" ", "_"), "drawable", context.getPackageName())));
        }
    }

    public void setItemEnabled(String name){
        for (GalleryItem item : items) {
            if (item.getName().equals(name)) {
                item.setActive(true);
                item.setBmp(BitmapFactory.decodeResource(context.getResources(), context.getResources().getIdentifier("icon_" + item.getName().toLowerCase().replace(" ", "_").replace(" ", "_") + "_active", "drawable", context.getPackageName())));
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void setItemDisabled(String name){
        for (GalleryItem item : items) {
            if (item.getName().equals(name)) {
                item.setActive(false);
                item.setBmp(BitmapFactory.decodeResource(context.getResources(), context.getResources().getIdentifier("icon_" + item.getName().toLowerCase().replace(" ", "_").replace(" ", "_"), "drawable", context.getPackageName())));
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void setItemsEnabled(List<String> names){
        if(items!= null && items.size() > 0){
            for (String name : names){
                setItemEnabled(name);
            }
        }else if(items != null && items.size() <= 0){
            setOtherItemsDisabled(null);
        }
    }

    public boolean isAnyItemActive() {
        for (GalleryItem item : items) {
            if (item.isActive()) return true;
        }
        return false;
    }

    public GalleryItem getActiveItem() {
        for (GalleryItem item : items) {
            if (item.isActive()) return item;
        }
        return null;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView picture;
        TextView name;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.gallery_item, parent, false);
            convertView.setTag(R.id.gallery_item_picture, convertView.findViewById(R.id.gallery_item_picture));
            convertView.setTag(R.id.gallery_item_text, convertView.findViewById(R.id.gallery_item_text));
        }

        picture = (ImageView) convertView.getTag(R.id.gallery_item_picture);
        name = (TextView) convertView.getTag(R.id.gallery_item_text);

        GalleryItem item = (GalleryItem) getItem(position);

        picture.setImageBitmap(item.getBmp());
        //name.setText(item.getName());

        return convertView;
    }
}
