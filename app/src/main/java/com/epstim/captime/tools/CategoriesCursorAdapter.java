package com.epstim.captime.tools;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.epstim.captime.FacebookOpenGraph.OGObject;
import com.epstim.captime.FacebookOpenGraph.SpendAction;
import com.epstim.captime.R;
import com.epstim.captime.activities.MainActivity;
import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.dbmodel.History;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.facebook.model.OpenGraphAction;
import com.facebook.model.OpenGraphObject;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Alan Żur on 2014-10-09.
 */
public class CategoriesCursorAdapter extends CursorAdapter {
    Context context;
    private LayoutInflater cursorInflater;
    int count;

    public CategoriesCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.context = context;
        cursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        count = 0;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return cursorInflater.inflate(R.layout.category_list_item, parent, false);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView date, duration;
        ImageButton delete,share;
        LinearLayout layout;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        final int id = cursor.getInt(cursor.getColumnIndex(History.C_ID));
        final int cId = cursor.getInt(cursor.getColumnIndex(History.C_CAT_ID));
        layout = (LinearLayout) view.findViewById(R.id.categoryItemLayout);
        if(count % 2 == 1){
            layout.setBackgroundColor(context.getResources().getColor(R.color.light_gray));
        }else{
            layout.setBackgroundColor(context.getResources().getColor(R.color.white_background));
        }
        count++;
        date = (TextView) view.findViewById(R.id.categoryItemDate);
        date.setText(sdf.format(new Date(cursor.getLong(cursor.getColumnIndex(History.C_START)))));
        Long time = cursor.getLong(cursor.getColumnIndex(History.C_STOP)) - cursor.getLong(cursor.getColumnIndex(History.C_START));
        duration = (TextView) view.findViewById(R.id.categoryItemDuration);
        duration.setText(Tools.formatTime(time));
        delete = (ImageButton) view.findViewById(R.id.categoryItemDeleteButton);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        HttpResponse response = JsonHelper.sendDELETE(JsonHelper.HOST + "records/id/" + id);
                        Log.d("DELETE", "host: " + JsonHelper.HOST + "records/id/" + id);
                        Log.d("DELETE", "Result code: " + response.getStatusLine().getStatusCode() + " result message: " + response.getStatusLine().getReasonPhrase());
                        if(response.getStatusLine().getStatusCode() == 204){
                            int result = context.getContentResolver().delete(Uri.parse(History.URI + "/" + id), History.C_ID + " = ?", new String[]{String.valueOf(id)});
                            Log.d("DELETE", "database: " + result);
                            MainActivity.application.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(context, "Success!", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                        String selection = History.C_CAT_ID + " = ?";
                        String[] selectionArgs = {String.valueOf(cId)};
                        final Cursor cursor = context.getContentResolver().query(
                                Uri.parse(History.URI + "/limit/10"),
                                History.getProjection(),
                                selection,
                                selectionArgs,
                                History.C_START + " DESC"
                        );
                        MainActivity.application.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swapCursor(cursor);
                                notifyDataSetInvalidated();
                                notifyDataSetChanged();
                            }
                        });
                    }
                }).start();
            }
        });

        Cursor categoryName = context.getContentResolver().query(
                Categories.URI,
                Categories.getProjection(),
                Categories.C_ID + " = ?",
                new String[]{String.valueOf(cId)},
                null
        );

        String categorySlug = null;
        if(categoryName.moveToFirst()){
            String catName = categoryName.getString(categoryName.getColumnIndex(Categories.C_NAME));
            categorySlug = categoryName.getString(categoryName.getColumnIndex(Categories.C_SLUG));
            if(categorySlug == null) categorySlug = catName.toLowerCase().replace(" ", "-");
            Tools.TimeContainer container = Tools.getTime(time);

            String jsonObject = getGraphObjectString(categorySlug, container);
            //String jsonObject = getGraphObjectJson(categorySlug, container);

            final Request request = getRequest(context, categorySlug, jsonObject, container);

            share = (ImageButton) view.findViewById(R.id.categoryItemShareButton);
            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Session.getActiveSession().isPermissionGranted("publish_actions")) request.executeAsync();
                    else Toast.makeText(context, "Permissions to publish on facebook not granted or not logged via Facebook", Toast.LENGTH_LONG).show();
                }
            });
        }
        categoryName.close();
    }

    private Request getRequest(final Context context, String categorySlug, String jsonObject, Tools.TimeContainer container) {
        final Bundle object = new Bundle();
        object.putString(categorySlug.replace("-","_"), jsonObject);
        object.putBoolean("fb:explicitly_shared", true);

        Request request = new Request(
                Session.getActiveSession(),
                "me/cap-time:spent",
                object,
                HttpMethod.POST,
                new Request.Callback() {
                    @Override
                    public void onCompleted(Response response) {
                        if(response != null && response.getError() != null) Log.d("Facebook Share", "Error message:" + response.getError().getErrorCode() + ", " + response.getError().getErrorMessage());
                        if(response != null && response.getGraphObject() != null){
                            Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );
        //request.setGraphObject(getGraphAction(categorySlug, container));

        return request;
    }

    private String getGraphObjectString(String categorySlug, Tools.TimeContainer container) {
        StringBuilder sb = new StringBuilder();
        sb.append("{")
                .append("\"").append("og:type").append("\":\"").append("cap-time:").append(categorySlug.replace("-", "_")).append("\",")
                .append("\"").append("og:url").append("\":\"").append("http://app.cap-time.com").append("\",")
                .append("\"").append("og:title").append("\":\"").append("Capture every moment! - Cap time!").append("\",")
                .append("\"").append("og:image").append("\":\"").append("http://captime.epst.im/assets/fb_icon/").append(categorySlug).append(".png").append("\",")
                .append("\"").append("og:description").append("\":\"").append("Track and check how much time you spend on particular activities, see what distracts you most and improve your productivity!").append("\",")
                .append("\"").append("cap-time:hours").append("\":\"").append(container.getHours()).append("\",")
                .append("\"").append("cap-time:minutes").append("\":\"").append(container.getMinutes()).append("\",")
                .append("\"").append("cap-time:seconds").append("\":\"").append(container.getSeconds()).append("\"")
        .append("}");

        return sb.toString();
    }

    private OpenGraphAction getGraphAction(String categorySlug, Tools.TimeContainer container) {
        SpendAction action = GraphObject.Factory.create(SpendAction.class);

        OGObject object = GraphObject.Factory.create(OGObject.class);
        object.setType("cap-time:" + categorySlug.replace("-", "_"));
        //object.setUrl("http://app.cap-time.com");
        object.setTitle("Capture every moment! - Cap time!");
        object.setDescription("Track and check how much time you spend on particular activities, see what distracts you most and improve your productivity!");
        List<String> imageUrls = new ArrayList<>();
        imageUrls.add("http://captime.epst.im/assets/fb_icon/" + categorySlug + ".png");
        object.setImageUrls(imageUrls);
        object.setHours(container.getHours());
        object.setMinutes(container.getMinutes());
        object.setSeconds(container.getSeconds());

        action.setProperty(":transport",object);
        action.setExplicitlyShared(true);

        return action;
    }

    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        if (getFilterQueryProvider() != null) { return getFilterQueryProvider().runQuery(constraint); }

        String selection = History.C_CAT_ID + " = ?";
        String[] selectionArgs = {constraint.toString()};

        return context.getContentResolver().query(
                Uri.parse(History.URI + "/limit/10"),
                History.getProjection(),
                selection,
                selectionArgs,
                History.C_START + " DESC"
        );
    }
}
