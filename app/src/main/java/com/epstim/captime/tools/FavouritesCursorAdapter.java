package com.epstim.captime.tools;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import com.epstim.captime.R;
import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.dbmodel.Favourites;

import java.util.ArrayList;

/**
 * Created by Alan Żur on 2014-09-30.
 */
public class FavouritesCursorAdapter extends CursorAdapter {

    public FavouritesCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.activities_grid_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        CustomImageView image = (CustomImageView) view.findViewById(R.id.activitiesGridItem);
        String name = cursor.getString(cursor.getColumnIndex(Categories.C_NAME));
        int fav = cursor.getInt(cursor.getColumnIndex(Categories.C_FAV));
        if(fav == 1) image.setImageResource(context.getResources().getIdentifier("icon_" + name.toLowerCase().replace(" ", "_") + "_active", "drawable", context.getPackageName()));
        else image.setImageResource(context.getResources().getIdentifier("icon_" + name.toLowerCase().replace(" ", "_") + "_inactive", "drawable", context.getPackageName()));
    }
}
