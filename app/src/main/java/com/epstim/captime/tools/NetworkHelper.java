package com.epstim.captime.tools;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Alan Żur on 2014-05-12.
 */
public class NetworkHelper {
    private static final String TAG = NetworkHelper.class.getSimpleName();

    public static boolean isConnected(final Context context) {
        final NetworkInfo info = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        final Future<Boolean> task = executorService.submit(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Looper.prepare();
                if (info == null) {
                    Toast.makeText(context, "Brak połączenia sieciowego!", Toast.LENGTH_LONG).show();
                    //Log.d(TAG, "No network connection");
                    return false;
                } else {
                    if (info.isConnected()) {
                        try {
                            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.onet.pl/").openConnection());
                            urlc.setRequestProperty("User-Agent", "Test");
                            urlc.setRequestProperty("Connection", "close");
                            urlc.setConnectTimeout(3000);
                            urlc.connect();
                            //Log.i(TAG, "Connected!");
                            return (urlc.getResponseCode() == 200);
                        } catch (IOException e) {
                            Toast.makeText(context, "Brak połączenia z serwerem!", Toast.LENGTH_LONG).show();
                            Log.e(TAG, "Error checking internet connection", e);
                        }
                        Toast.makeText(context, "Brak połączenia z serwerem!", Toast.LENGTH_LONG).show();
                        return false;
                    } else {
                        Toast.makeText(context, "Brak połączenia z siecią!", Toast.LENGTH_LONG).show();
                        return false;
                    }
                }
            }
        });

        try {
            return task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }
}
