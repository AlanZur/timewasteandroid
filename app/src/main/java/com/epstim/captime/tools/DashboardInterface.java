package com.epstim.captime.tools;

import java.util.List;

/**
 * Created by Alan Żur on 2014-07-31.
 */
public interface DashboardInterface {
    public void setItemsText(List<String> categories, List<String> texts);
    public void setItemsInactive();
    public void setRefreshing(Boolean isRefreshing);
}
