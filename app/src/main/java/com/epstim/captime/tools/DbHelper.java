package com.epstim.captime.tools;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.dbmodel.Favourites;
import com.epstim.captime.dbmodel.History;

/**
 * Created by Alan Żur on 2014-07-25.
 */
public class DbHelper extends SQLiteOpenHelper {
    public static final String TAG = DbHelper.class.getSimpleName();
    public static final String CATEGORIES_DOWNLOAD_KEY = "categories_downloaded";
    private static final String DATABASE_NAME = "captime.db";
    private static final int DATABASE_VERSION = 1;
    private Context context;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Categories.onCreate(db);
        History.onCreate(db);
        Favourites.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Categories.onUpdate(db, oldVersion, newVersion);
        History.onUpdate(db, oldVersion, newVersion);
        Favourites.onUpdate(db);
    }
}
