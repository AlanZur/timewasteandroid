package com.epstim.captime.tools;

import android.content.Context;
import android.content.Intent;

import com.epstim.captime.activities.MainActivity;

/**
 * Created by Alan Żur on 2014-08-27.
 */
public class NetworkMonitor extends Thread {
    public static final String NO_INTERNET_ACTION = "com.epstim.captime.ACTION_NO_INTERNET";
    public static final String INTERNET_ACTION = "com.epstim.captime.ACTION_INTERNET";
    private static final String TAG = NetworkMonitor.class.getSimpleName();
    private boolean run = true;
    private Context context;
    private Intent intent;

    private static class NetworkMonitorHolder{
        static final NetworkMonitor INSTANCE = new NetworkMonitor();
    }

    protected NetworkMonitor() {
        super();
        this.setName("NetworkMonitor");
        context = MainActivity.getAppContext();
    }

    @Override
    public void run() {
        while(isRun()){
            context.sendBroadcast(new Intent(NetworkHelper.isConnected(context) ? INTERNET_ACTION : NO_INTERNET_ACTION));
            //Log.d(TAG, NetworkHelper.isConnected(context) ? INTERNET_ACTION : NO_INTERNET_ACTION);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized boolean isRun() {
        return run;
    }

    public synchronized void setRun(boolean run) {
        this.run = run;
    }

    public static NetworkMonitor getInstance() {
        return NetworkMonitorHolder.INSTANCE;
    }
}

