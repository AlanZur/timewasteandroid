package com.epstim.captime.fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;

import com.epstim.captime.activities.MainActivity;
import com.epstim.captime.R;
import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.tools.PagerAdapter;
import com.epstim.captime.tools.TimerService;
import com.epstim.captime.tools.Tools;

/**
 * A simple {@link Fragment} subclass.
 */
public class StatisticsFragment extends Fragment {
    public static final String TAG = StatisticsFragment.class.getSimpleName();
    private MainActivity activity;

    public StatisticsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_statistics, container, false);
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.pager);
        TabHost tabHost = (TabHost) view.findViewById(R.id.tabHost);
        tabHost.setup();
        HorizontalScrollView scrollView = (HorizontalScrollView) view.findViewById(R.id.scrollView);
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager(), getActivity(), tabHost, viewPager, scrollView);
        adapter.addTab(tabHost.newTabSpec(activity.getString(R.string.overall_tab).toLowerCase()).setIndicator(activity.getString(R.string.overall_tab)), OverallStatisticsFragment.class, null);
        //adapter.addTab(tabHost.newTabSpec(activity.getString(R.string.last_activities).toLowerCase()).setIndicator(activity.getString(R.string.last_activities)), LastActivitiesFragment.class, null);

        Cursor cursor = getActivity().getContentResolver().query(Categories.URI, Categories.getProjection(), null, null, null);

        if (cursor.moveToFirst()) {
            do {
                Bundle bundle = new Bundle();
                String name = cursor.getString(cursor.getColumnIndex(Categories.C_NAME));
                int id = cursor.getInt(cursor.getColumnIndex(Categories.C_ID));
                bundle.putString(CategoryFragment.ARG_NAME, name);
                bundle.putInt(CategoryFragment.ARG_ID, id);
                adapter.addTab(tabHost.newTabSpec(name.toLowerCase()).setIndicator(name), CategoryFragment.class, bundle);
            } while (cursor.moveToNext());
        }
        cursor.close();

        Log.d(TAG, String.valueOf(adapter.getCount()));

        return view;
    }


}
