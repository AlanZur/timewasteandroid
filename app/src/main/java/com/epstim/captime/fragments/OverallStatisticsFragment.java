package com.epstim.captime.fragments;


import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.epstim.captime.R;
import com.epstim.captime.dbmodel.History;
import com.epstim.captime.tools.Tools;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.interfaces.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Highlight;
import com.github.mikephil.charting.utils.Legend;

import java.util.ArrayList;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class OverallStatisticsFragment extends Fragment{
    private TextView overallTextView, monthlyTextView, weeklyTextView, dailyTextView;
    PieChart pieChartAll, pieChartMonthly ,pieChartWeekly, pieChartDaily;
    PieChartData pieChartDataOverall, pieChartDataWeekly, pieChartDataDaily, pieChartDataMonthly;
    OnChartValueSelectedListener overallListener = new OnChartValueSelectedListener() {
        @Override
        public void onValueSelected(Entry e, int dataSetIndex) {
            overallTextView.setVisibility(View.VISIBLE);
            if(e != null) overallTextView.setText(pieChartAll.getXValue(e.getXIndex()) + " : " + Tools.formatTime((long) e.getVal()*1000));
        }

        @Override
        public void onNothingSelected() {
            overallTextView.setVisibility(View.GONE);
        }
    };

    OnChartValueSelectedListener monthlyListener = new OnChartValueSelectedListener() {
        @Override
        public void onValueSelected(Entry e, int dataSetIndex) {
            monthlyTextView.setVisibility(View.VISIBLE);
            if(e != null)monthlyTextView.setText(pieChartAll.getXValue(e.getXIndex()) + " : " + Tools.formatTime((long) e.getVal()*1000));
        }

        @Override
        public void onNothingSelected() {
            monthlyTextView.setVisibility(View.GONE);
        }
    };

    OnChartValueSelectedListener weeklyListener = new OnChartValueSelectedListener() {
        @Override
        public void onValueSelected(Entry e, int dataSetIndex) {
            weeklyTextView.setVisibility(View.VISIBLE);
            if(e != null)weeklyTextView.setText(pieChartAll.getXValue(e.getXIndex()) + " : " + Tools.formatTime((long) e.getVal()*1000));
        }

        @Override
        public void onNothingSelected() {
            weeklyTextView.setVisibility(View.GONE);
        }
    };

    OnChartValueSelectedListener dailyListener = new OnChartValueSelectedListener() {
        @Override
        public void onValueSelected(Entry e, int dataSetIndex) {
            dailyTextView.setVisibility(View.VISIBLE);
            if(e != null)dailyTextView.setText(pieChartAll.getXValue(e.getXIndex()) + " : " + Tools.formatTime((long) e.getVal()*1000));
        }

        @Override
        public void onNothingSelected() {
            dailyTextView.setVisibility(View.GONE);
        }
    };

    public OverallStatisticsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overall_statistics, container, false);
        overallTextView = (TextView) view.findViewById(R.id.overallTextView);
        monthlyTextView = (TextView) view.findViewById(R.id.monthlyTextView);
        weeklyTextView = (TextView) view.findViewById(R.id.weeklyTextView);
        dailyTextView = (TextView) view.findViewById(R.id.dailyTextView);
        initPieChartOverall(view);
        initPieChartMonthly(view);
        initPieChartWeekly(view);
        initPieChartDaily(view);
        return view;
    }

    private void initPieChartOverall(View view) {
        pieChartAll = (PieChart) view.findViewById(R.id.pieChartAll);
        pieChartAll.setDescription("");
        pieChartAll.setCenterText(getActivity().getString(R.string.pie_chart_overall_center_text));
        pieChartAll.setUsePercentValues(true);
        pieChartAll.setDrawXValues(false);
        pieChartAll.highlightValues(null);

        pieChartDataOverall = new PieChartData().invoke(
                Uri.parse(History.URI + "/overall"),
                new String[]{"sum(h.stop - h.start)/1000 total", "c.name", "h.cat_id"},
                History.C_STOP + " > 0",
                null,
                "");
        PieData chartData = pieChartDataOverall.getChartData();

        pieChartAll.setData(chartData);
        Legend legend = pieChartAll.getLegend();
        if(legend != null) {
            legend.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        }
        pieChartAll.setOnChartValueSelectedListener(overallListener);
        pieChartAll.invalidate();
    }

    private void initPieChartMonthly(View view){
        pieChartMonthly = (PieChart) view.findViewById(R.id.pieChartMonthly);
        pieChartMonthly.setDescription("");
        pieChartMonthly.setCenterText(getActivity().getString(R.string.pie_chart_monthly_center_text));
        pieChartMonthly.setUsePercentValues(true);
        pieChartMonthly.setDrawXValues(false);

        long monthlyRange = new Date().getTime() - CategoryFragment.MONTH_MILLIS;
        pieChartDataMonthly = new PieChartData().invoke(
                Uri.parse(History.URI + "/overall"),
                new String[]{"sum(h.stop - h.start)/1000 total", "c.name", "h.cat_id"},
                "h." + History.C_START + " >= ? AND h." + History.C_STOP + " > ?",
                new String[]{String.valueOf(monthlyRange), String.valueOf(0l)},
                "");
        PieData chartData = pieChartDataMonthly.getChartData();

        pieChartMonthly.setData(chartData);
        Legend legend = pieChartMonthly.getLegend();
        if(legend != null) {
            legend.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        }

        pieChartMonthly.setOnChartValueSelectedListener(monthlyListener);
    }

    private void initPieChartWeekly(View view){
        pieChartWeekly = (PieChart) view.findViewById(R.id.pieChartWeek);
        pieChartWeekly.setDescription("");
        pieChartWeekly.setCenterText(getActivity().getString(R.string.pie_chart_weekly_center_text));
        pieChartWeekly.setUsePercentValues(true);
        pieChartWeekly.setDrawXValues(false);

        long weeklyRange = new Date().getTime() - CategoryFragment.WEEK_MILLIS;
        pieChartDataWeekly = new PieChartData().invoke(
                Uri.parse(History.URI + "/overall"),
                new String[]{"sum(h.stop - h.start)/1000 total", "c.name", "h.cat_id"},
                "h." + History.C_START + " >= ? AND h." + History.C_STOP + " > ?",
                new String[]{String.valueOf(weeklyRange), String.valueOf(0l)},
                "");
        PieData chartData = pieChartDataWeekly.getChartData();

        pieChartWeekly.setData(chartData);
        Legend legend = pieChartWeekly.getLegend();
        if(legend != null) {
            legend.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        }

        pieChartWeekly.setOnChartValueSelectedListener(weeklyListener);
    }

    private void initPieChartDaily(View view){
        pieChartDaily = (PieChart) view.findViewById(R.id.pieChartToday);
        pieChartDaily.setDescription("");
        pieChartDaily.setCenterText(getActivity().getString(R.string.pie_chart_daily_center_text));
        pieChartDaily.setUsePercentValues(true);
        pieChartDaily.setDrawXValues(false);

        long dailyRange = new Date().getTime() - CategoryFragment.DAY_MILLIS;
        pieChartDataDaily = new PieChartData().invoke(
                Uri.parse(History.URI + "/overall"),
                new String[]{"sum(h.stop - h.start)/1000 total", "c.name", "h.cat_id"},
                "h." + History.C_START + " >= ? AND h." + History.C_STOP + " > ?",
                new String[]{String.valueOf(dailyRange), String.valueOf(0l)},
                "");
        PieData chartData = pieChartDataDaily.getChartData();

        pieChartDaily.setData(chartData);
        Legend legend = pieChartDaily.getLegend();
        if(legend != null) {
            legend.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        }

        pieChartDaily.setOnChartValueSelectedListener(dailyListener);
    }

    private class PieChartData {
        private Cursor cursor;
        private ArrayList<String> labels;
        private ColorTemplate colorTemplate;
        private PieData chartData;
        private String[] labelsArray;

        public ArrayList<String> getLabels() {
            return labels;
        }

        public ColorTemplate getColorTemplate() {
            return colorTemplate;
        }

        public PieData getChartData() {
            return chartData;
        }

        public String[] getLabelsArray() {
            return labelsArray;
        }

        public PieChartData invoke(Uri uri, String[] projection, String selection, String[] selectionArgs, String title) {
            Cursor cursor = getActivity().getContentResolver().query(uri, projection, selection, selectionArgs, null);
            ArrayList<Entry> entries = new ArrayList<>();
            ArrayList<Integer> colors = new ArrayList<>();
            labels = new ArrayList<>();
            int index = 0;
            if (cursor.moveToFirst()) {
                do {
                    String temp = cursor.getString(1);
                    if(temp != null) {
                        entries.add(new Entry(cursor.getFloat(0), index));
                        labels.add(temp);
                        colors.add(
                                getResources().getColor(
                                        getResources().getIdentifier(
                                                "color_" + temp.toLowerCase().replace(" ", "_"),
                                                "color",
                                                getActivity().getPackageName()
                                        )
                                )
                        );
                    }
                    index++;
                } while (cursor.moveToNext());
            }
            PieDataSet set = new PieDataSet(entries, title);

            set.setSliceSpace(3f);
            set.setColors(colors);
            chartData = new PieData(labels, set);
            cursor.close();
            return this;
        }
    }
}
