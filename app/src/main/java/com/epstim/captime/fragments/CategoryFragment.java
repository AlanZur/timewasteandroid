package com.epstim.captime.fragments;


import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.epstim.captime.activities.MainActivity;
import com.epstim.captime.R;
import com.epstim.captime.dbmodel.History;
import com.epstim.captime.tools.CategoriesCursorAdapter;
import com.epstim.captime.tools.Tools;
import com.facebook.Session;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.Legend;
import com.github.mikephil.charting.utils.XLabels;
import com.github.mikephil.charting.utils.YLabels;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment {
    public static final String TAG = CategoryFragment.class.getSimpleName();
    public static final String ARG_NAME = "name";
    public static final String ARG_ID = "id";
    public static final long HOUR_MILLIS = 3600000l;
    public static final long DAY_MILLIS = 86400000l;
    public static final long WEEK_MILLIS = 604800000l;
    public static final long MONTH_MILLIS = 2592000000l;
    public String name;
    public int id;
    MainActivity activity;

    public CategoryFragment() {
        // Required empty public constructor
    }

    public static CategoryFragment newInstance(String name, int id) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NAME, name);
        args.putInt(ARG_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.name = getArguments().getString(ARG_NAME);
            this.id = getArguments().getInt(ARG_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        initBarChart(view);
        ListView listView = (ListView) view.findViewById(R.id.categoryListView);
        String selection = History.C_CAT_ID + " = ?";
        String[] selectionArgs = {String.valueOf(id)};
        Cursor cursor = getActivity().getContentResolver().query(
                Uri.parse(History.URI + "/limit/10"),
                History.getProjection(),
                selection,
                selectionArgs,
                History.C_START + " DESC"
        );
        listView.setAdapter(new CategoriesCursorAdapter(getActivity(), cursor, 0));
        return view;
    }

    private void initBarChart(View view) {
        BarChart barChart = (BarChart) view.findViewById(R.id.categoryBarChart);
        barChart.setDrawYValues(true);
        barChart.setDescription("");
        barChart.setDrawGridBackground(false);
        barChart.setDrawHorizontalGrid(true);
        barChart.setDrawVerticalGrid(true);
        barChart.setDrawBorder(false);
        barChart.setDrawBarShadow(false);
        XLabels xl = barChart.getXLabels();
        xl.setPosition(XLabels.XLabelPosition.BOTTOM);
        xl.setCenterXLabelText(true);
        YLabels yl = barChart.getYLabels();
        yl.setLabelCount(8);
        yl.setPosition(YLabels.YLabelPosition.BOTH_SIDED);
        barChart.setData(getBarData());
        Legend l = barChart.getLegend();
        if(l != null) {
            l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
            l.setFormSize(8f);
            l.setXEntrySpace(4f);
        }
    }

    private BarData getBarData() {
        long weeklyRange = new Date().getTime() - WEEK_MILLIS;
        BarData data = null;
        ArrayList<String> xVals = new ArrayList<>();
        ArrayList<BarEntry> yVals = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE");

        for(int i = 0; i < 8; i++){
            Date date = new Date();
            Long time = date.getTime();
            time = time - (24*60*60*1000*(7-i));
            date.setTime(time);
            Long start = getStartOfDay(date).getTime();
            Long end = getEndOfDay(date).getTime();
            String selection = "(start BETWEEN ? AND ?) AND cat_id = ?";
            String [] selectionArgs = {String.valueOf(start), String.valueOf(end), String.valueOf(id)};
            Cursor cursor = getActivity().getContentResolver().query(History.URI, History.getProjection(), selection, selectionArgs, History.C_START + " DESC");
            long length = 0;
            String dayName = sdf.format(date);
            if(cursor.moveToFirst()){
                do{
                    length += cursor.getLong(cursor.getColumnIndex(History.C_STOP)) - cursor.getLong(cursor.getColumnIndex(History.C_START));
                }while(cursor.moveToNext());
            }
            yVals.add(new BarEntry(length/(1000*60),i));
            xVals.add(dayName);
            cursor.close();
        }

        BarDataSet dataSet = new BarDataSet(yVals, activity.getString(R.string.chart_bar_time_in_minutes));
        dataSet.setBarSpacePercent(35f);
        dataSet.setColor(
                getResources().getColor(
                        getResources().getIdentifier(
                                "color_" + name.toLowerCase().replace(" ","_"),
                                "color",
                                getActivity().getPackageName()
                        )
                )
        );

        ArrayList<BarDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSet);

        data = new BarData(xVals, dataSets);

        return data;
    }

    public Date getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public Date getEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

}
