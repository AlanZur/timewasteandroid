package com.epstim.captime.fragments;


import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.epstim.captime.activities.EditActivities;
import com.epstim.captime.activities.MainActivity;
import com.epstim.captime.R;
import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.tools.DashboardInterface;
import com.epstim.captime.tools.GalleryItem;
import com.epstim.captime.tools.SwipeDashboardListAdapter;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends android.support.v4.app.Fragment implements DashboardInterface{
    public static final String TAG = DashboardFragment.class.getSimpleName();
    List<GalleryItem> items;
    MainActivity activity;
    SwipeRefreshLayout refreshLayout;
    private SwipeListView swipeListView;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (MainActivity) getActivity();
        items = new ArrayList<>();
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        setSwipeListView(new SwipeListView(getActivity(), R.id.back, R.id.front));
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        int width = size.x;
        getSwipeListView().setSwipeMode(SwipeListView.SWIPE_MODE_RIGHT);
        getSwipeListView().setSwipeActionRight(SwipeListView.SWIPE_ACTION_REVEAL);
        getSwipeListView().setSwipeCloseAllItemsWhenMoveList(true);
        getSwipeListView().setOffsetLeft(width * 0.33f);
        getSwipeListView().setOffsetRight(width * 0.33f);
        buildItems();
        getSwipeListView().setAdapter(new SwipeDashboardListAdapter(getActivity(), DashboardFragment.this, R.layout.dashboard_row, items));
        getSwipeListView().setSwipeListViewListener(new BaseSwipeListViewListener() {
            @Override
            public void onClickFrontView(int position) {
                if (items.get(position).getId() == -1) {
                    startActivity(new Intent(getActivity(), EditActivities.class));
                    return;
                }
                if (items.get(position).getTimer() != null)
                    activity.stopTimer(items.get(position).getName());
                else activity.startTimer(items.get(position).getName());
            }

            @Override
            public void onClickBackView(int position) {
                getSwipeListView().closeAnimate(position);
            }
        });
        refreshLayout  = (SwipeRefreshLayout) view.findViewById(R.id.swipeLayout);
        try {
            Field field = SwipeRefreshLayout.class.getDeclaredField("mDistanceToTriggerSync");
            field.setAccessible(true);
            float distanceToTriggerSync = width / 3;
            field.setFloat(refreshLayout, distanceToTriggerSync);
        } catch (Exception e) {
            e.printStackTrace();
        }
        refreshLayout.addView(getSwipeListView(), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        refreshLayout.setOnRefreshListener((MainActivity)getActivity());
        refreshLayout.setColorScheme(R.color.swipe_color1, R.color.swipe_color2, R.color.swipe_color3, R.color.swipe_color4);

        return view;
    }

    public void buildItems() {
        Cursor cursor = getActivity().getContentResolver().query(Categories.URI, Categories.getProjection(), null, null, null);
        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(cursor.getColumnIndex(Categories.C_NAME));
                int id = cursor.getInt(cursor.getColumnIndex(Categories.C_ID));
                int fav = cursor.getInt(cursor.getColumnIndex(Categories.C_FAV));
                if(fav == 1) items.add(new GalleryItem(name,
                                BitmapFactory.decodeResource(
                                        getActivity().getResources(), getActivity().getResources().getIdentifier(
                                                "icon_" + name.toLowerCase().replace(" ","_"),
                                                "drawable",
                                                getActivity().getPackageName())
                                ),
                                id,
                                null)
                );
            } while (cursor.moveToNext());
        }
        items.add(new GalleryItem(
                getString(R.string.add),
                BitmapFactory.decodeResource(getResources(), R.drawable.ic_add_grey),
                -1,
                null
        ));
        cursor.close();
    }

    public void refresh(){
        items.clear();
        buildItems();
        ((SwipeDashboardListAdapter) getSwipeListView().getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void setItemsText(List<String> categories, List<String> texts) {
        ((SwipeDashboardListAdapter) getSwipeListView().getAdapter()).setItemsActive(categories, texts);
    }

    @Override
    public void setItemsInactive() {
        ((SwipeDashboardListAdapter) getSwipeListView().getAdapter()).setItemsInactive();
    }

    @Override
    public void setRefreshing(Boolean isRefreshing) {
        refreshLayout.setRefreshing(isRefreshing);
    }

    public SwipeListView getSwipeListView() {
        return swipeListView;
    }

    public void setSwipeListView(SwipeListView swipeListView) {
        this.swipeListView = swipeListView;
    }
}

