package com.epstim.captime.fragments;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.epstim.captime.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class NoInternetConnectionFragment extends Fragment {


    public static final String TAG = NoInternetConnectionFragment.class.getSimpleName();

    public NoInternetConnectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_no_internet_connection, container, false);
    }


}
