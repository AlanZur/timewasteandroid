package com.epstim.captime.fragments;



import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.epstim.captime.activities.MainActivity;
import com.epstim.captime.R;
import com.epstim.captime.dbmodel.History;
import com.epstim.captime.jsonmodel.JsonHistory;
import com.epstim.captime.tools.SwipeActivitiesListAdapter;
import com.facebook.Session;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class LastActivitiesFragment extends Fragment{
    SwipeListView list;
    MainActivity activity;

    public LastActivitiesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        activity = (MainActivity) getActivity();


        List<JsonHistory> items = new ArrayList<JsonHistory>();
        Cursor cursor = getActivity().getContentResolver().query(Uri.parse(History.URI + "/limit/10"), History.getProjection(), null, null, History.C_START + " DESC");
        if(cursor.moveToFirst()){
            do{
                JsonHistory item = new JsonHistory();
                item.id = cursor.getInt(cursor.getColumnIndex(History.C_ID));
                item.latitude = cursor.getDouble(cursor.getColumnIndex(History.C_LAT));
                item.longitude = cursor.getDouble(cursor.getColumnIndex(History.C_LONG));
                item.start = cursor.getLong(cursor.getColumnIndex(History.C_START));
                item.stop = cursor.getLong(cursor.getColumnIndex(History.C_STOP));
                item.categoryId = cursor.getInt(cursor.getColumnIndex(History.C_CAT_ID));
                items.add(item);
            }while(cursor.moveToNext());
        }
        list = new SwipeListView(getActivity(), R.id.back, R.id.front);
        container.addView(list, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        final float width = container.getWidth();
        list.setSwipeMode(SwipeListView.SWIPE_MODE_RIGHT);
        list.setSwipeActionRight(SwipeListView.SWIPE_ACTION_REVEAL);
        list.setOffsetLeft(width * 0.33f);
        list.setOffsetRight(width * 0.33f);

        list.setAdapter(new SwipeActivitiesListAdapter(getActivity(), LastActivitiesFragment.this, R.layout.last_activities_row, items));
        list.setSwipeListViewListener(new BaseSwipeListViewListener(){
            @Override
            public void onClickFrontView(int position) {
                list.openAnimate((int) (list.getTop() - (width *0.33)));
            }

            @Override
            public void onClickBackView(int position) {
                list.closeAnimate(position);
            }
        });
        View view = inflater.inflate(R.layout.fragment_last_activities, container, false);
        cursor.close();
        return view;
    }
}
