package com.epstim.captime.dbmodel;

import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.epstim.captime.tools.DatabaseContentProvider;

/**
 * Created by Alan Żur on 2014-07-25.
 */
public class History {
    public static final String T_NAME = "history";
    public static final String BASE_PATH = T_NAME;
    public static final Uri URI = Uri.parse("content://" + DatabaseContentProvider.AUTHORITY + "/" + BASE_PATH);
    public static final String C_ID = "_id";
    public static final String C_START = "start";
    public static final String C_STOP = "stop";
    public static final String C_LAT = "latitude";
    public static final String C_LONG = "longitude";
    public static final String C_CAT_ID = "cat_id";
    private static final String CREATE_TABLE = "create table " + T_NAME + " ("
            + C_ID + " INTEGER NOT NULL PRIMARY KEY, "
            + C_START + " NUMERIC NOT NULL, "
            + C_STOP + " NUMERIC, "
            + C_LAT + " REAL, "
            + C_LONG + " REAL, "
            + C_CAT_ID + " INTEGER NOT NULL, "
            + "FOREIGN KEY (" + C_CAT_ID + ") REFERENCES " + History.T_NAME + " (" + History.C_ID + ")"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE);
    }

    public static void onUpdate(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("drop table if exists " + T_NAME);
        onCreate(database);
    }

    public static String[] getProjection() {
        return new String[]{C_ID, C_START, C_STOP, C_LAT, C_LONG, C_CAT_ID};
    }
}
