package com.epstim.captime.dbmodel;

import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.epstim.captime.tools.DatabaseContentProvider;

/**
 * Created by Alan Żur on 2014-09-29.
 */
public class Favourites {
    public static final String T_NAME = Favourites.class.getSimpleName();
    public static final Uri URI = Uri.parse(DatabaseContentProvider.AUTHORITY + "/" + T_NAME);
    public static final String C_ID = "_id";
    public static final String C_CAT_ID = "cat_id";

    private static final String CREATE_TABLE = "CREATE TABLE " + T_NAME + " (" +
            C_ID + " INTEGER NOT NULL PRIMARY KEY, " +
            C_CAT_ID + " INTEGER NOT NULL);";

    public static void onCreate(SQLiteDatabase db){
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpdate(SQLiteDatabase db){
        db.execSQL("DROP TABLE IF EXISTS " + T_NAME);
        onCreate(db);
    }

    public static String[] getProjection(){
        return new String[]{C_ID, C_CAT_ID};
    }
}
