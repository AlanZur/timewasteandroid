package com.epstim.captime.dbmodel;

import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.epstim.captime.tools.DatabaseContentProvider;

import java.io.Serializable;

/**
 * Created by Alan Żur on 2014-07-25.
 */
public class Categories implements Serializable {
    public static final String T_NAME = "categories";
    public static final String BASE_PATH = T_NAME;
    public static final Uri URI = Uri.parse("content://" + DatabaseContentProvider.AUTHORITY + "/" + BASE_PATH);
    public static final String C_ID = "_id";
    public static final String C_NAME = "name";
    public static final String C_COLOR = "color";
    public static final String C_SLUG = "slug";
    public static final String C_FAV = "favourite";
    private static final String CREATE_TABLE = "create table " + T_NAME + "("
            + C_ID + " INTEGER NOT NULL, "
            + C_NAME + " TEXT NOT NULL, "
            + C_COLOR + " TEXT, "
            + C_SLUG + " TEXT, "
            + C_FAV + " INTEGER, "
            + " PRIMARY KEY (" + C_ID + " ASC)"
            + ");";


    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE);
    }

    public static void onUpdate(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("drop table if exists " + T_NAME);
        onCreate(database);
    }

    public static String[] getProjection() {
        return new String[]{C_ID, C_NAME, C_COLOR, C_SLUG, C_FAV};
    }

}
