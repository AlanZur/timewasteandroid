package com.epstim.captime.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;

import com.epstim.captime.activities.MainActivity;
import com.epstim.captime.R;
import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.jsonmodel.JsonHistory;
import com.epstim.captime.jsonmodel.JsonUser;
import com.epstim.captime.tools.GPSTracker;
import com.epstim.captime.tools.JsonHelper;
import com.epstim.captime.tools.TimerService;
import com.epstim.captime.tools.Tools;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

/**
 * Implementation of App Widget functionality.
 */
public class ActivityWidget extends AppWidgetProvider {
    public static final String TOAST_ACTION = ActivityWidget.class.getName() + ".TOAST_ACTION";
    public static final String SERVICE_ACTION = ActivityWidget.class.getName() + ".SERVICE_ACTION";
    public static final String EXTRA_ITEM = ActivityWidget.class.getName() + ".EXTRA_ITEM";
    private static GPSTracker gpsTracker;
    private static int currentIndex = -1;
    private static JsonHistory historyObject;
    private static Intent timerService;
    private String baseUrl = JsonHelper.HOST;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        if(gpsTracker == null) {
            gpsTracker = new GPSTracker(context, GPSTracker.ProviderType.NETWORK);
            gpsTracker.start();
        }
        // There may be multiple widgets active, so update all of them
        final int N = appWidgetIds.length;
        for (int i=0; i<N; i++) {
            updateAppWidget(context, appWidgetManager, appWidgetIds[i]);
        }
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        AppWidgetManager mgr = AppWidgetManager.getInstance(context);
        if (intent.getAction().equals(TimerService.ACTION)){
            timerService = intent;
            if(intent.hasExtra(TimerService.HISTORY_ARG)) {
                historyObject = (JsonHistory) intent.getSerializableExtra(TimerService.HISTORY_ARG);
                currentIndex = historyObject.categoryId;
                RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.activity_widget);

                if (timerService != null && timerService.hasExtra(TimerService.CATEGORY_ARG) && timerService.hasExtra(TimerService.TIME_ARG))
                    views.setTextViewText(
                            R.id.widgetTimerTextView,
                            String.format(
                                    context.getString(R.string.timer_text),
                                    timerService.getStringExtra(TimerService.CATEGORY_ARG),
                                    Tools.formatTime(timerService.getLongExtra(TimerService.TIME_ARG, -1))
                            ));
                ComponentName componentName = new ComponentName(context, ActivityWidget.class);
                AppWidgetManager.getInstance(context).updateAppWidget(componentName, views);
            }
        }else if (intent.getAction().equals(TOAST_ACTION)) {
            final Gson gson = new GsonBuilder().create();
            int appWidgetId = intent.getIntExtra(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            final int viewIndex = intent.getIntExtra(EXTRA_ITEM, 0) + 1;
            if(Tools.isServiceRunning(context, TimerService.class.getName()) && currentIndex == viewIndex){
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        stopCurrentActivity(context, gson);
                    }
                }).start();
                return;
            }else if(Tools.isServiceRunning(context, TimerService.class.getName()) && currentIndex != viewIndex){
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        startAnotherActivity(context, gson);
                    }
                }).start();
            }else
            currentIndex = viewIndex;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    startNewActivity(viewIndex, context, gson);
                }
            }).start();
        }
        super.onReceive(context, intent);
    }

    private synchronized void startAnotherActivity(Context context, Gson gson) {
        context.stopService(timerService);
        String jsonBody = gson.toJson(historyObject, JsonHistory.class);
        JsonHelper.sendPUT(baseUrl + "records/id/" + historyObject.id, jsonBody);
    }

    private synchronized void stopCurrentActivity(Context context, Gson gson) {
        context.stopService(new Intent(context, TimerService.class));
        String jsonBody = gson.toJson(historyObject, JsonHistory.class);
        String response = JsonHelper.sendPUT(baseUrl + "records/id/" + historyObject.id, jsonBody);
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.activity_widget);
        views.setTextViewText(R.id.widgetTimerTextView, context.getString(R.string.app_name ));
        ComponentName componentName = new ComponentName(context, ActivityWidget.class);
        AppWidgetManager.getInstance(context).updateAppWidget(componentName, views);
        Log.d("Widget", response);
    }

    private synchronized void startNewActivity(int viewIndex, Context context, Gson gson) {
        String response;
        String catName = "";
        String selection = Categories.C_ID + " = ?";
        String [] selectionArgs = {String.valueOf(viewIndex)};
        Cursor cursor = context.getContentResolver().query(Categories.URI, Categories.getProjection(), selection, selectionArgs, null);
        if (cursor.moveToFirst()) {
            catName = cursor.getString(cursor.getColumnIndex(Categories.C_NAME));
        }
        cursor.close();

        Location location = null;
        if(gpsTracker != null) {
            location = gpsTracker.getLocation();
        }
        historyObject = new JsonHistory();

        if (location != null) {
            historyObject.latitude = location.getLatitude();
            historyObject.longitude = location.getLongitude();
        }
        historyObject.start = new Date().getTime();
        historyObject.categoryId = viewIndex;
        SharedPreferences preferences = context.getSharedPreferences(MainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        historyObject.userId = preferences.getString(JsonUser.ID_KEY, "-1");
        historyObject.fbid = preferences.getString(JsonUser.FBID_KEY, "-1");

        String jsonBody = gson.toJson(historyObject, JsonHistory.class);

        response = JsonHelper.sendPOST(baseUrl + "records", jsonBody);
        Log.d("WIDGET", "body: " + jsonBody + "\nresponse: " + response);
        JsonHistory temp = gson.fromJson(response, JsonHistory.class);
        historyObject.id = temp.id;

        timerService = new Intent(context, TimerService.class);
        timerService.putExtra(TimerService.CATEGORY_ARG, catName);
        timerService.putExtra(TimerService.HISTORY_ARG, historyObject);

        context.startService(timerService);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
        if(gpsTracker == null) {
            gpsTracker = new GPSTracker(context, GPSTracker.ProviderType.NETWORK);
            gpsTracker.start();
        }
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
            int appWidgetId) {
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.activity_widget);

        // Here we setup the intent which points to the StackViewService
        // which will
        // provide the views for this collection.
        Intent intent = new Intent(context, WidgetService.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

        // When intents are compared, the extras are ignored, so we need to
        // embed the extras
        // into the data so that the extras will not be ignored.
        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
        views.setRemoteAdapter(R.id.widgetCategories, intent);

        // The empty view is displayed when the collection has no items. It
        // should be a sibling
        // of the collection view.
        views.setEmptyView(R.id.widgetCategories, R.id.gallery_item_picture);

        // Here we setup the a pending intent template. Individuals items of
        // a collection
        // cannot setup their own pending intents, instead, the collection
        // as a whole can
        // setup a pending intent template, and the individual items can set
        // a fillInIntent
        // to create unique before on an item to item basis.
        Intent toastIntent = new Intent(context, ActivityWidget.class);
        toastIntent.setAction(ActivityWidget.TOAST_ACTION);
        toastIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        toastIntent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
        PendingIntent toastPendingIntent = PendingIntent.getBroadcast(context, 0, toastIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setPendingIntentTemplate(R.id.widgetCategories, toastPendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }
}


