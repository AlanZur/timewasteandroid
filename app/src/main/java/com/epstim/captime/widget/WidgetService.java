package com.epstim.captime.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.epstim.captime.R;
import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.tools.GalleryItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alan Żur on 2014-08-20.
 */
public class WidgetService extends RemoteViewsService{

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new WidgetRemoteViewsFactory(this.getApplicationContext(), intent);
    }
}

class WidgetRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory{
    private List<GalleryItem> items = new ArrayList<GalleryItem>();
    private Context context;
    private int appWidgetId;
    private Cursor cursor;

    public WidgetRemoteViewsFactory(Context context, Intent intent){
        this.context = context;
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
    }

    @Override
    public void onCreate() {
        cursor = context.getContentResolver().query(Categories.URI, Categories.getProjection(), null, null, Categories.C_ID);
        if(cursor.moveToFirst()){
            do{
                String name = cursor.getString(cursor.getColumnIndex(Categories.C_NAME));
                int id = cursor.getInt(cursor.getColumnIndex(Categories.C_ID));
                items.add(new GalleryItem(
                        name,
                        BitmapFactory.decodeResource(
                                context.getResources(),
                                context.getResources().getIdentifier(
                                        "icon_" + name.toLowerCase(),
                                        "drawable",
                                        context.getPackageName())), id, null));
            }while(cursor.moveToNext());
        }
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_gallery_item);
        views.setImageViewBitmap(R.id.widgetItemImage, items.get(position).getBmp());
        views.setTextViewText(R.id.widgetItemTextView, items.get(position).getName());
        Bundle extras = new Bundle();
        extras.putInt(ActivityWidget.EXTRA_ITEM, position);
        Intent fillInIntent = new Intent();
        fillInIntent.putExtras(extras);
        views.setOnClickFillInIntent(R.id.widgetItemImage, fillInIntent);
        return views;
    }

    @Override
    public void onDataSetChanged() {

    }

    @Override
    public void onDestroy() {
        cursor.close();
        items.clear();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}