package com.epstim.captime.jsonmodel;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by Alan Żur on 2014-08-05.
 */
public class JsonSaltResponse implements Serializable {
    public String salt;
    public String error;

    @Override
    public String toString(){
        return new Gson().toJson(this, JsonHistory.class);
    }
}
