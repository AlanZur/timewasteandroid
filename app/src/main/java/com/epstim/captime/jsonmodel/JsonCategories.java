package com.epstim.captime.jsonmodel;

import java.io.Serializable;

/**
 * Created by Alan Żur on 2014-07-31.
 */
public class JsonCategories implements Serializable {
    public int id;
    public String name;
}
