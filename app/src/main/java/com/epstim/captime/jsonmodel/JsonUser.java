package com.epstim.captime.jsonmodel;

import com.facebook.model.GraphUser;
import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by Alan Żur on 2014-07-31.
 */
public class JsonUser implements Serializable {
    public static final String NAME_KEY = "name";
    public static final String ID_KEY = "id";
    public static final String SURNAME_KEY = "surname";
    public static final String EMAIL_KEY = "email";
    public static final String FBID_KEY = "fbid";
    public static final String SALT_KEY = "salt";
    public static final String PASSWORD_KEY = "password";

    public String id;

    public String name;

    public String surname;

    public String email;

    public String fbid;

    public String salt;

    public String password;

    public String status;

    public String error;

    public static JsonUser toJsonUser(GraphUser user){
        JsonUser temp = new JsonUser();
        temp.name = user.getFirstName();
        temp.surname = user.getLastName();
        temp.fbid = user.getId();
        if(user.getProperty("email") != null) temp.email = (String) user.getProperty("email");
        return temp;
    }

    @Override
    public String toString(){
        return new Gson().toJson(this, JsonUser.class);
    }
}
