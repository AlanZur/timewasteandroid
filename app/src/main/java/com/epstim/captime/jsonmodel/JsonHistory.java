package com.epstim.captime.jsonmodel;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Alan Żur on 2014-07-31.
 */
public class JsonHistory implements Serializable {
    public int id;

    @SerializedName("user_id")
    public String userId;

    public String fbid;

    @SerializedName("category_id")
    public int categoryId;

    public long start;

    public long stop;

    public double latitude;

    public double longitude;

    public String address;

    public String status;

    public String slug;

    @Override
    public String toString(){
        return new Gson().toJson(this, JsonHistory.class);
    }
}
