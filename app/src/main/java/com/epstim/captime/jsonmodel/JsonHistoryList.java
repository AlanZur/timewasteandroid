package com.epstim.captime.jsonmodel;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Alan Żur on 2014-09-01.
 */
public class JsonHistoryList extends ArrayList<JsonHistory> implements Serializable{

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (JsonHistory item : this){
            sb.append(item.toString()).append("\n");
        }
        return sb.toString();
    }
}
