package com.epstim.captime.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.epstim.captime.BuildConfig;
import com.epstim.captime.R;
import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.fragments.CategoryFragment;
import com.epstim.captime.fragments.DashboardFragment;
import com.epstim.captime.fragments.LastActivitiesFragment;
import com.epstim.captime.fragments.NoInternetConnectionFragment;
import com.epstim.captime.fragments.StatisticsFragment;
import com.epstim.captime.jsonmodel.JsonHistory;
import com.epstim.captime.jsonmodel.JsonHistoryList;
import com.epstim.captime.jsonmodel.JsonUser;
import com.epstim.captime.tools.ApplicationInterface;
import com.epstim.captime.tools.GPSTracker;
import com.epstim.captime.tools.JsonHelper;
import com.epstim.captime.tools.NetworkHelper;
import com.epstim.captime.tools.NetworkMonitor;
import com.epstim.captime.tools.TimerService;
import com.epstim.captime.tools.ToastAdListener;
import com.epstim.captime.tools.Tools;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MainActivity extends FragmentActivity implements ApplicationInterface, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static Intent timerService;
    private static final int GMS_STATUS_CODE = 1090;
    public static MainActivity application;
    public static UiLifecycleHelper uiLifecycleHelper;
    private GPSTracker gpsTracker;
    private SharedPreferences preferences;
    private SharedPreferences dbPreferences;
    private JsonHistoryList activities;
    private SparseArray<String> categories;
    private Bundle savedInstanceState;
    private ProgressDialog progressDialog;
    private AdView adView;
    private FrameLayout fragmentContainer;
    LinearLayout layout;
    private static Context context;
    private boolean isConnected, initUI = false;
    private NetworkMonitor networkMonitor;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateTimer(intent);
        }
    };

    private BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isConnected(NetworkHelper.isConnected(getApplicationContext()));
        }
    };

    private BroadcastReceiver connectedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isConnected(true);
        }
    };

    private BroadcastReceiver notConnectedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isConnected(false);
        }
    };


    private void updateTimer(Intent intent) {
        activities = new JsonHistoryList();
        setDashboardItemsDisabled();
        ArrayList<JsonHistory> temp = (ArrayList<JsonHistory>) intent.getSerializableExtra(TimerService.ACTIVITY_LIST_ARG);
        if(temp != null){
            if(temp.size() != activities.size()) progressDialog.dismiss();
            activities.clear();
            activities.addAll(temp);
            if(activities.size() > 0){
                ArrayList<String> labels = new ArrayList<String>();
                ArrayList<String> timerTexts = new ArrayList<String>();
                ArrayList<Integer> ids = new ArrayList<>();
                for(JsonHistory item: activities){
                    labels.add(getCategories().get(item.categoryId));
                    timerTexts.add(Tools.formatTime(new Date().getTime() - item.start));
                    ids.add(item.categoryId);
                }
                addTempCategories(labels, timerTexts, ids);
                StringBuilder stringBuilder = new StringBuilder();
                for(int i = 0 ; i < activities.size() ; i++){
                    Long timeInMillis = new Date().getTime() - activities.get(i).start;
                    stringBuilder.append(getCategories().get(activities.get(i).categoryId)).append(": ").append(Tools.formatTime(timeInMillis)).append("\n");
                }
                setDashboardItemsEnabled(labels, timerTexts);
            }
        }
        if(temp != null && temp.size() == 0){
            if(progressDialog.isShowing()) progressDialog.dismiss();
        }
        if(activities.size() == 0){
            setDashboardItemsDisabled();
        }
    }

    private void setDashboardItemsDisabled() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (fragment != null && fragment instanceof DashboardFragment) {
            ((DashboardFragment) fragment).setItemsInactive();
        }
    }

    private void setDashboardItemsEnabled(List<String> names, List<String> texts){
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (fragment != null && fragment instanceof DashboardFragment) {
            if(names.size() != texts.size()) throw new IllegalArgumentException("Category names does not equals timer texts");
            else{
                ((DashboardFragment) fragment).setItemsText(names, texts);
            }
        }
    }

    private void addTempCategories(ArrayList<String> names, ArrayList<String> texts, ArrayList<Integer> ids){
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (fragment != null && fragment instanceof DashboardFragment) {
            if(names.size() != texts.size()) throw new IllegalArgumentException("Category names does not equals timer texts");
            else{
                for(Integer id : ids){
                    ContentValues values = new ContentValues();
                    values.put(Categories.C_FAV, 1);
                    int result = getContentResolver().update(
                            Categories.URI,
                            values,
                            Categories.C_ID + " = ?",
                            new String[]{String.valueOf(id)}
                    );
                }
                ((DashboardFragment)fragment).refresh();
            }
        }
    }

    private void onSessionStateChange(Session session, SessionState sessionState, Exception exception) {

    }

    public static Context getAppContext() {
        return MainActivity.context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity.context = getApplicationContext();
        MainActivity.application = MainActivity.this;
        this.savedInstanceState = savedInstanceState;
        if(getActionBar() != null) getActionBar().setDisplayShowTitleEnabled(false);
        setContentView(R.layout.activity_main);
        Crashlytics.start(this);
        try {
            gpsTracker = new GPSTracker(getApplicationContext(), GPSTracker.ProviderType.NETWORK); // URZĄDZENIE
            gpsTracker.start();
        }catch(Exception ex) {
            ex.printStackTrace();
            gpsTracker = new GPSTracker(getApplicationContext(), GPSTracker.ProviderType.GPS); // EMULATOR
            gpsTracker.start();
        }

        initUI();

        setCategories(new SparseArray<String>());
        Cursor cursor = getContentResolver().query(Categories.URI, Categories.getProjection(), null, null, null);
        if(cursor.moveToFirst()){
            do{
                int id = cursor.getInt(cursor.getColumnIndex(Categories.C_ID));
                String label = cursor.getString(cursor.getColumnIndex(Categories.C_NAME));
                getCategories().put(id, label);
            }while(cursor.moveToNext());
        }
        cursor.close();

        int statusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if(statusCode != ConnectionResult.SUCCESS){
            GooglePlayServicesUtil.getErrorDialog(statusCode, this, GMS_STATUS_CODE);
        }

        Session.StatusCallback callback = new Session.StatusCallback() {
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                onSessionStateChange(session, state, exception);
            }
        };

        uiLifecycleHelper = new UiLifecycleHelper(this, callback);
        uiLifecycleHelper.onCreate(savedInstanceState);

        if(!NetworkMonitor.getInstance().isAlive()) NetworkMonitor.getInstance().start();

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        showDashboardFragment();
    }

    private void initUI() {
        fragmentContainer = (FrameLayout) findViewById(R.id.fragmentContainer);
        if(BuildConfig.FLAVOR == "free") {
            adView = new AdView(context);
            adView.setAdUnitId(getString(R.string.ad_unit_id));
            adView.setAdSize(AdSize.SMART_BANNER);
            adView.setAdListener(new ToastAdListener(context));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layout = (LinearLayout) findViewById(R.id.mainContainer);
            layout.addView(adView, params);
            AdRequest.Builder builder = new AdRequest.Builder();
            Location location = gpsTracker.getLocation();
            if (location != null) builder.setLocation(location);
            adView.loadAd(builder.build());
        }
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle(getString(R.string.dialog_title_communicate_with_server));
        progressDialog.setMessage(getString(R.string.dialog_message_please_wait));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);

        setCategories(new SparseArray<String>());
        Cursor cursor = getContentResolver().query(Categories.URI, Categories.getProjection(), null, null, null);
        if(cursor.moveToFirst()){
            do{
                int id = cursor.getInt(cursor.getColumnIndex(Categories.C_ID));
                String label = cursor.getString(cursor.getColumnIndex(Categories.C_NAME));
                getCategories().put(id, label);
            }while(cursor.moveToNext());
        }
        cursor.close();

        initUI = true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                break;
            case R.id.action_statistics:
                showStatisticsFragment();
                break;
            case R.id.action_dashboard:
                showDashboardFragment();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((FrameLayout)findViewById(R.id.fragmentContainer)).removeAllViews();
        showDashboardFragment();
        uiLifecycleHelper.onResume();
        registerReceivers();
        if(!Tools.isServiceRunning(context, TimerService.class.getName())){
            if(timerService != null )startService(timerService);
            else{
                timerService = new Intent(context, TimerService.class);
                startService(timerService);
            }
        }

        Session session = Session.getActiveSession();
        onSessionStateChange(session, session.getState(), null);
        if (adView != null) {
            adView.resume();
        }
        if(Tools.isServiceRunning(getAppContext(), TimerService.class.getName())){
            onRefresh();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        uiLifecycleHelper.onPause();
        unregisterReceivers();
        gpsTracker.stop();
        NetworkMonitor.getInstance().interrupt();
        if (adView != null) {
            adView.pause();
        }
    }

    private void registerReceivers() {
        registerReceiver(broadcastReceiver, new IntentFilter(TimerService.ACTION));
        registerReceiver(networkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        registerReceiver(connectedReceiver, new IntentFilter(NetworkMonitor.INTERNET_ACTION));
        registerReceiver(notConnectedReceiver, new IntentFilter(NetworkMonitor.NO_INTERNET_ACTION));
    }

    private void unregisterReceivers() {
        unregisterReceiver(broadcastReceiver);
        unregisterReceiver(networkReceiver);
        unregisterReceiver(connectedReceiver);
        unregisterReceiver(notConnectedReceiver);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiLifecycleHelper.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GMS_STATUS_CODE && resultCode != RESULT_OK){
            Toast.makeText(context, data.getDataString(), Toast.LENGTH_LONG).show();
            return;
        }
        Session session = Session.getActiveSession();
        onSessionStateChange(session, session.getState(), null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(Tools.isServiceRunning(context, TimerService.class.getName())) stopService(timerService);
        uiLifecycleHelper.onDestroy();
        if (adView != null) {
            adView.destroy();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("is_connected", isConnected);
        super.onSaveInstanceState(outState);
        uiLifecycleHelper.onSaveInstanceState(outState);
    }



    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        ((FrameLayout)findViewById(R.id.fragmentContainer)).removeAllViews();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if(fragment instanceof DashboardFragment){
            super.onBackPressed();
            return;
        }
        if(fragment instanceof StatisticsFragment){
            showDashboardFragment();
            return;
        }
        if(fragment instanceof LastActivitiesFragment){
            showDashboardFragment();
            return;
        }
        if(manager.getBackStackEntryCount() > 0){
            manager.popBackStackImmediate();
        }
    }

    @Override
    public void showLoginFragment() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

    }

    @Override
    public void showDashboardFragment() {
        ((FrameLayout)findViewById(R.id.fragmentContainer)).removeAllViews();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, new DashboardFragment(), DashboardFragment.TAG)
                    .commitAllowingStateLoss();

    }

    @Override
    public void showStatisticsFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, new StatisticsFragment(), StatisticsFragment.TAG)
                .commit();
    }

    @Override
    public void showActivityFragment(String name, int id) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, CategoryFragment.newInstance(name, id), CategoryFragment.TAG)
                .addToBackStack(null)
                .commit();
    }

    private JsonHistory buildHistoryObject(int catId){
        JsonHistory jsonHistory = new JsonHistory();
        if(gpsTracker != null){
            Location location = gpsTracker.getLocation();
            if(location != null){
                jsonHistory.latitude = location.getLatitude();
                jsonHistory.longitude = location.getLongitude();
            }
        }
        jsonHistory.categoryId = catId;
        jsonHistory.start = new Date().getTime()/1000;
        jsonHistory.userId = preferences.getString(JsonUser.ID_KEY, "-1");
        return jsonHistory;
    }

    @Override
    public void startTimer(String category) {
        if(!progressDialog.isShowing()) progressDialog.show();
        int id = getCategoryId(category);
        if(id >= 0){
            if(activities.size() < 2 ){
                JsonHistory temp = buildHistoryObject(id);
                Intent intent = new Intent();
                intent.putExtra(TimerService.HISTORY_ARG, temp);
                intent.setAction(TimerService.ACTION_ADD);
                sendBroadcast(intent);
            }else{
                Toast.makeText(context, getString(R.string.main_activity_too_many_activities), Toast.LENGTH_LONG).show();
            }
        }
    }

    public static int getCategoryId(String category) {
        String selection = Categories.C_NAME + " = ?";
        String[] selectionArgs = {category};
        int id = -1;
        Cursor cursor = context.getContentResolver().query(Categories.URI, Categories.getProjection(), selection, selectionArgs, null);
        if(cursor.moveToFirst()){
            id = cursor.getInt(cursor.getColumnIndex(Categories.C_ID));
        }
        cursor.close();
        return id;
    }

    @Override
    public void stopTimer(String category) {
        if(!progressDialog.isShowing()) progressDialog.show();
        int id = getCategoryId(category);
        if(id>=0){
            Intent intent = new Intent();
            intent.putExtra(TimerService.CATEGORY_ID_ARG, id);
            intent.setAction(TimerService.ACTION_DELETE);
            sendBroadcast(intent);
        }
    }

    @Override
    public void saveUserData(JsonUser user) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear().apply();
        editor.putString(JsonUser.ID_KEY, user.id);
        if (user.fbid != null) editor.putString(JsonUser.FBID_KEY, user.fbid);
        editor.putString(JsonUser.NAME_KEY, user.name);
        editor.putString(JsonUser.SURNAME_KEY, user.surname);
        editor.putString(JsonUser.EMAIL_KEY, user.email);
        editor.putString(JsonUser.SALT_KEY, user.salt);
        editor.putString(JsonUser.PASSWORD_KEY, user.password);
        editor.apply();
    }

    @Override
    public void downloadRecords() {

    }

    @Override
    public SharedPreferences getPreferences() {
        return this.preferences;
    }

    @Override
    public void isConnected(Boolean isConnected) {
        this.isConnected = isConnected;
        if(isConnected){
            if(BuildConfig.FLAVOR == "free") adView.setVisibility(View.VISIBLE);
            if(preferences.contains(JsonUser.ID_KEY) && preferences.contains(JsonUser.FBID_KEY)){
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
                if(fragment != null && fragment instanceof NoInternetConnectionFragment) getSupportFragmentManager().popBackStack();
            }
        }else{
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
            if(fragment != null && !(fragment instanceof NoInternetConnectionFragment)) showNoConnectionScreen();
            if(BuildConfig.FLAVOR == "free") adView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNoConnectionScreen() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, new NoInternetConnectionFragment(), NoInternetConnectionFragment.TAG)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public synchronized void onRefresh() {
        if(!Tools.isServiceRunning(getApplicationContext(), TimerService.class.getName())) {
            setDashboardItemsDisabled();
        }else{
            activities = new JsonHistoryList();
            setDashboardItemsDisabled();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                String result = JsonHelper.sendGET(JsonHelper.HOST + "records/user_id/"
                        + preferences.getString(JsonUser.ID_KEY, "-1") + "/stopped/0");
                if(result != null) {
                    Log.d(TAG, result);
                    if (result.startsWith("[")) {
                        Gson gson = new Gson();
                        activities = gson.fromJson(result, JsonHistoryList.class);
                        timerService = new Intent(context, TimerService.class);
                        if(activities != null && activities.size() > 0){
                            for(JsonHistory item : activities){
                                item.start *= 1000;
                                item.stop *= 1000;
                            }
                            timerService.putExtra(TimerService.ACTIVITY_LIST_ARG, activities);
                            if(Tools.isServiceRunning(context, TimerService.class.getName())) context.stopService(timerService);
                            context.startService(timerService);
                        }
                    }else if(result.startsWith("{")){
                        if(Tools.isServiceRunning(context, TimerService.class.getName())){
                            sendBroadcast(new Intent().setAction(TimerService.ACTION_CLEAR));
                            if(activities != null) activities.clear();
                        }
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, getString(R.string.internal_server_error), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
                final Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (fragment instanceof DashboardFragment)
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((DashboardFragment) fragment).setRefreshing(false);
                        }
                    });
            }
        }).start();
    }

    public SparseArray<String> getCategories() {
        return categories;
    }

    public void setCategories(SparseArray<String> categories) {
        this.categories = categories;
    }

}

/**
 *  https://github.com/dspoko/FacebookTest
 **/