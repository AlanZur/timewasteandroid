package com.epstim.captime.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.epstim.captime.R;
import com.epstim.captime.jsonmodel.JsonUser;
import com.epstim.captime.tools.JsonHelper;
import com.google.gson.Gson;

public class EditProfileActivity extends Activity {
    private static final String TAG = EditProfileActivity.class.getSimpleName();
    private LayoutHolder holder;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        holder = new LayoutHolder();
        holder.email = (EditText) findViewById(R.id.editProfileActivityEmailEditText);
        holder.email.setHint(preferences.getString(JsonUser.EMAIL_KEY, ""));
        holder.name = (EditText) findViewById(R.id.editProfileActivityNameEditText);
        holder.name.setHint(preferences.getString(JsonUser.NAME_KEY, ""));
        holder.surname = (EditText) findViewById(R.id.editProfileActivitySurnameEditText);
        holder.surname.setHint(preferences.getString(JsonUser.SURNAME_KEY,""));
        holder.password = (EditText) findViewById(R.id.editProfileActivityPasswordEditText);
        holder.repeatPassword = (EditText) findViewById(R.id.editProfileActivityPasswordRepeatEditText);
        holder.repeatPassword.setTransformationMethod(new PasswordTransformationMethod());
        holder.save = (LinearLayout) findViewById(R.id.editProfileActivityRegisterLayout);
        holder.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonUser user = new JsonUser();
                user.id = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(JsonUser.ID_KEY, "-1");
                if(holder.email.getText().length() > 0) user.email = holder.email.getText().toString();
                if(holder.name.getText().length() > 0) user.name = holder.name.getText().toString();
                if(holder.surname.getText().length() > 0) user.surname = holder.surname.getText().toString();
                if(holder.password.getText().length() > 0) {
                    if(holder.repeatPassword.getText().toString().equals(holder.repeatPassword.getText().toString())) {
                        user.password = holder.password.getText().toString();
                    }else{
                        Toast.makeText(getApplicationContext(), getString(R.string.incorect_password), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                new EditProfileAsyncTask().execute(user);
            }
        });

    }

    private class LayoutHolder{
        EditText email, name, surname, password, repeatPassword;
        LinearLayout save;
    }

    private class EditProfileAsyncTask extends AsyncTask<JsonUser, Void, String>{
        Gson gson;

        @Override
        protected String doInBackground(JsonUser... params) {
            gson = new Gson();
            //params[0].id = null;
            String jsonBody = gson.toJson(params[0], JsonUser.class);
            Log.d(TAG, jsonBody);
            String response = JsonHelper.sendPUT("http://apitimewaste.epst.im/users/id/" + params[0].id, jsonBody);
            Log.d(EditProfileAsyncTask.class.getSimpleName(), response);
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            if(!s.isEmpty()){
                JsonUser user = gson.fromJson(s, JsonUser.class);
                if(user != null) {
                    if(user.status != null){
                        Toast.makeText(EditProfileActivity.this, getString(R.string.edit_failed), Toast.LENGTH_SHORT).show();
                    }
                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                    if (user.name != null) {
                        editor.putString(JsonUser.NAME_KEY, user.name);
                        holder.name.setHint(user.name);
                    }
                    if (user.surname != null){
                        editor.putString(JsonUser.SURNAME_KEY, user.surname);
                        holder.surname.setHint(user.surname);
                    }
                    if (user.email != null){
                        editor.putString(JsonUser.EMAIL_KEY, user.email);
                        holder.email.setHint(user.email);
                    }
                    if (user.password != null) {
                        editor.putString(JsonUser.PASSWORD_KEY, user.password);
                    }
                    if (user.salt != null) editor.putString(JsonUser.SALT_KEY, user.salt);
                    editor.commit();
                    Toast.makeText(EditProfileActivity.this, getString(R.string.account_edited), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
