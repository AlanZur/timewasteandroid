package com.epstim.captime.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.epstim.captime.R;
import com.epstim.captime.tools.DbHelper;
import com.facebook.Session;

public class SettingsActivity extends Activity implements View.OnClickListener{
    LayoutHolder holder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        holder = new LayoutHolder();
        holder.edit = (LinearLayout) findViewById(R.id.settingsActivityEditProfile);
        holder.edit.setOnClickListener(this);
        holder.activities = (LinearLayout) findViewById(R.id.settingsActivityActivities);
        holder.activities.setOnClickListener(this);
        holder.logout = (LinearLayout) findViewById(R.id.settingsActivityLogout);
        holder.logout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.settingsActivityEditProfile:
                startActivity(new Intent(getApplicationContext(), EditProfileActivity.class));
                break;
            case R.id.settingsActivityActivities:
                startActivity(new Intent(getApplicationContext(), EditActivities.class));
                break;
            case R.id.settingsActivityLogout:
                logout();
                break;
            default:
                break;
        }
    }


    private void logout() {
        Session session = Session.getActiveSession();
        DbHelper helper = new DbHelper(getApplicationContext());
        if (session != null) {
            if (!session.isClosed()) {
                session.closeAndClearTokenInformation();
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().commit();
                helper.onUpgrade(helper.getWritableDatabase(), 1, 2);
            }
        } else {
            session = new Session(getApplicationContext());
            Session.setActiveSession(session);

            session.closeAndClearTokenInformation();
            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().commit();
            helper.onUpgrade(helper.getWritableDatabase(), 1, 2);
        }
        startActivity(new Intent(getApplicationContext(), LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }


    private class LayoutHolder{
        LinearLayout edit, activities, logout;
    }
}
