package com.epstim.captime.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.epstim.captime.R;
import com.epstim.captime.jsonmodel.JsonUser;
import com.epstim.captime.tools.JsonHelper;
import com.epstim.captime.tools.Tools;
import com.google.gson.Gson;

public class RegisterActivity extends Activity {
    LayoutHolder holder;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        preferences  = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        holder = new LayoutHolder();
        holder.email = (EditText) findViewById(R.id.registerActivityEmailEditText);
        holder.name = (EditText) findViewById(R.id.registerActivityNameEditText);
        holder.surname = (EditText) findViewById(R.id.registerActivitySurnameEditText);
        holder.password = (EditText) findViewById(R.id.registerActivityPasswordEditText);
        holder.password.setTransformationMethod(new PasswordTransformationMethod());
        holder.repeatPassword = (EditText) findViewById(R.id.registerActivityPasswordRepeatEditText);
        holder.repeatPassword.setTransformationMethod(new PasswordTransformationMethod());
        holder.register = (LinearLayout) findViewById(R.id.registerActivityRegisterLayout);
        holder.register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean canSendRegistration = true;
                canSendRegistration = Tools.isValidEmail(holder.email.getText());
                canSendRegistration = canSendRegistration && (holder.name.length() > 0);
                canSendRegistration = canSendRegistration && (holder.surname.length() > 0);
                canSendRegistration = canSendRegistration && (holder.password.length() > 0);
                canSendRegistration = canSendRegistration && (holder.repeatPassword.length() > 0);
                canSendRegistration = canSendRegistration && (holder.repeatPassword.getText().toString().equals(holder.password.getText().toString()));
                if(canSendRegistration){
                    new RegisterAsyncTask().execute();
                }
            }
        });
    }

    private class LayoutHolder{
        EditText email, name, surname, password, repeatPassword;
        LinearLayout register;
    }

    private void saveUserData(JsonUser user){
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear().apply();
        editor.putString(JsonUser.ID_KEY, user.id);
        if (user.fbid != null) editor.putString(JsonUser.FBID_KEY, user.fbid);
        editor.putString(JsonUser.NAME_KEY, user.name);
        editor.putString(JsonUser.SURNAME_KEY, user.surname);
        editor.putString(JsonUser.EMAIL_KEY, user.email);
        editor.putString(JsonUser.SALT_KEY, user.salt);
        editor.putString(JsonUser.PASSWORD_KEY, user.password);
        editor.apply();
    }


    private class RegisterAsyncTask extends AsyncTask<Void, Void, Integer>{
        private final String TAG = RegisterAsyncTask.class.getSimpleName();
        int code;
        AlertDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new AlertDialog.Builder(RegisterActivity.this)
                    .setTitle(getString(R.string.dialog_title_please_wait))
                    .setMessage(getString(R.string.dialog_title_communicate_with_server))
                    .show();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            code = -1;
            JsonUser user = new JsonUser();
            user.email = holder.email.getText().toString();
            user.name = holder.name.getText().toString();
            user.surname = holder.surname.getText().toString();
            user.password = holder.password.getText().toString();
            Gson gson = new Gson();
            String jsonBody = gson.toJson(user, JsonUser.class);
            String result = JsonHelper.sendPOST(JsonHelper.HOST + "/users", jsonBody);
            Log.d(TAG, "result: " + result);
            user = gson.fromJson(result, JsonUser.class);
            if(user != null && user.status == null){
                saveUserData(user);
                code = 1;
            }else code = 0;
            return code;
        }

        @Override
        protected void onPostExecute(Integer code) {
            if(dialog.isShowing()) dialog.dismiss();
            switch (code){
                case -1:
                    Toast.makeText(RegisterActivity.this, getString(R.string.toast_unknown_error), Toast.LENGTH_LONG).show();
                    break;
                case 0:
                    Toast.makeText(RegisterActivity.this, getString(R.string.toast_user_exists), Toast.LENGTH_LONG).show();
                    break;
                case 1:
                    Toast.makeText(RegisterActivity.this, getString(R.string.toast_account_created), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        }
    }
}
