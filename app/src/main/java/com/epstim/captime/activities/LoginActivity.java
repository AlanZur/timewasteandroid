package com.epstim.captime.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.epstim.captime.R;
import com.epstim.captime.jsonmodel.JsonSaltResponse;
import com.epstim.captime.jsonmodel.JsonUser;
import com.epstim.captime.tools.DbHelper;
import com.epstim.captime.tools.JsonHelper;
import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LoginActivity extends Activity implements View.OnClickListener {
    private static final String TAG = LoginActivity.class.getSimpleName();
    UiLifecycleHelper uiLifecycleHelper;

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };
    Session fbSession;
    ProgressDialog pleaseWaitDialog;
    EditText loginEditText, passwordEditText;
    LinearLayout weblayout, facebooklayout, registerLayout;
    ImageView loginImage, passwordImage;
    SharedPreferences preferences;
    GraphUser facebookUser;

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (session != null && session.isOpened()) {
            makeMeRequest(session);
        }
    }

    private void makeMeRequest(Session session) {
        Request.newMeRequest(session, new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser user, Response response) {
                if(user != null){
                    facebookUser = user;
                    new FacebookLoginAsyncTask().execute(JsonUser.toJsonUser(user));
                }
            }
        }).executeAsync();
    }

    private void startSplashActivity(Intent intent){
        printPreferences();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.facebook.samples.hellofacebook",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        fbSession = Session.openActiveSessionFromCache(LoginActivity.this);

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        uiLifecycleHelper = new UiLifecycleHelper(this, callback);
        uiLifecycleHelper.onCreate(savedInstanceState);

        pleaseWaitDialog = new ProgressDialog(LoginActivity.this);
        pleaseWaitDialog.setTitle(getString(R.string.dialog_title_please_wait));
        pleaseWaitDialog.setMessage(getString(R.string.dialog_message_login));

        loginImage = (ImageView) findViewById(R.id.loginActivityLoginImageView);
        passwordImage = (ImageView) findViewById(R.id.loginActivityPasswordImageView);

        loginEditText = (EditText) findViewById(R.id.loginActivityLoginEditText);
        /*loginEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) loginImage.setVisibility(View.GONE);
                else {
                    if (loginEditText.getText().toString().length() <= 0)
                        loginImage.setVisibility(View.VISIBLE);
                }
            }
        });*/
        passwordEditText = (EditText) findViewById(R.id.loginActivityPasswordEditText);
        passwordEditText.setTransformationMethod(new PasswordTransformationMethod());
        /*passwordEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) passwordImage.setVisibility(View.GONE);
                else{
                    if(passwordEditText.getText().toString().length() <= 0) passwordImage.setVisibility(View.VISIBLE);
                }
            }
        });*/
        if(preferences.contains(JsonUser.EMAIL_KEY) && preferences.contains(JsonUser.PASSWORD_KEY)){
            loginEditText.setText(preferences.getString(JsonUser.EMAIL_KEY,""));
            passwordEditText.setText(preferences.getString(JsonUser.PASSWORD_KEY,""));
            new WebLoginAsyncTask().execute(loginEditText.getText().toString(), passwordEditText.getText().toString());
        }

        weblayout = (LinearLayout) findViewById(R.id.loginActivityWebLoginLayout);
        weblayout.setOnClickListener(this);

        facebooklayout = (LinearLayout) findViewById(R.id.loginActivityFacebookLoginLayout);
        facebooklayout.setOnClickListener(this);

        registerLayout = (LinearLayout) findViewById(R.id.loginActivityRegisterLayout);
        registerLayout.setOnClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiLifecycleHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginActivityFacebookLoginLayout:
                Log.d("LOGIN", "Facebook login clicked");
                if (fbSession != null && fbSession.isOpened()) {
                    makeMeRequest(fbSession);
                    Log.i("Facebook Login State == >", "Facebook Login State");
                } else {
                    if (fbSession == null) {
                        fbSession = new Session(getApplicationContext());
                    }
                        Session.setActiveSession(fbSession);
                        ConnectToFacebook();
                        Log.i("Facebook not Login State == >", "Facebook Not login State");
                    }
                break;
            case R.id.loginActivityWebLoginLayout:
                boolean login = !loginEditText.getText().toString().isEmpty();
                boolean password = !passwordEditText.getText().toString().isEmpty();
                if(login && password){
                    new WebLoginAsyncTask().execute(loginEditText.getText().toString(), passwordEditText.getText().toString());
                    Log.d("LOGIN", "Login allowed");
                }else{
                    if(!login) loginEditText.setBackgroundResource(R.drawable.red_stroke);
                    else loginEditText.setBackgroundResource(R.color.white_background);
                    if(!password) passwordEditText.setBackgroundResource(R.drawable.red_stroke);
                    else passwordEditText.setBackgroundResource(R.color.white_background);
                }
                break;
            case  R.id.loginActivityRegisterLayout:
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        uiLifecycleHelper.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        uiLifecycleHelper.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uiLifecycleHelper.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiLifecycleHelper.onSaveInstanceState(outState);
    }

    private void ConnectToFacebook() {
        Session session = Session.getActiveSession();
        if(!session.isOpened() && !session.isClosed()){
            Session.OpenRequest newSession = new Session.OpenRequest(LoginActivity.this);
            //newSession.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
            newSession.setPermissions("email", "publish_actions", "publish_stream");
            session.openForPublish(newSession);
            Session.OpenRequest request = new Session.OpenRequest(LoginActivity.this);
            request.setPermissions("email", "publish_actions", "publish_stream");
        }else{
            Session.openActiveSession(LoginActivity.this, true, callback);
        }
    }

    private void saveUserData(JsonUser user){
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear().apply();
        editor.putString(JsonUser.ID_KEY, user.id);
        if (user.fbid != null) editor.putString(JsonUser.FBID_KEY, user.fbid);
        editor.putString(JsonUser.NAME_KEY, user.name);
        editor.putString(JsonUser.SURNAME_KEY, user.surname);
        editor.putString(JsonUser.EMAIL_KEY, user.email);
        editor.putString(JsonUser.SALT_KEY, user.salt);
        editor.putString(JsonUser.PASSWORD_KEY, user.password);
        editor.apply();
    }

    private void printPreferences(){
        Map<String, ?> keys = preferences.getAll();
        for(Map.Entry<String, ?> entry : keys.entrySet()){
            Log.d("PREFERENCES", entry.getKey() + ": " + entry.getValue().toString());
        }
    }

    private class WebLoginAsyncTask extends AsyncTask<String, Void, String> {
        private final String TAG = WebLoginAsyncTask.class.getSimpleName();

        @Override
        protected void onPreExecute() {
            if(!pleaseWaitDialog.isShowing()) pleaseWaitDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = JsonHelper.sendGET(JsonHelper.HOST + "/login/email/" + params[0]);
            Log.d(TAG, "Result: " + result);
            Gson gson = new GsonBuilder().create();
            JsonSaltResponse response = gson.fromJson(result, JsonSaltResponse.class);
            if (response != null) {
                if (response.salt != null) {
                    String passwordWithSalt = params[1] + response.salt;
                    String hash = new String(Hex.encodeHex(DigestUtils.sha(passwordWithSalt)));
                    result = JsonHelper.sendGET(JsonHelper.HOST + "/login/email/" + params[0] + "/hash/" + hash);
                    JsonUser user = gson.fromJson(result, JsonUser.class);
                    if (user.error != null) return getString(R.string.bad_password);
                    user.password = params[1];
                    saveUserData(user);
                    return String.format(getString(R.string.welcome_username), user.name);
                }
                if (response.error != null) {
                    return response.error;
                }
            }
            return "User registered with Facebook";
        }

        @Override
        protected void onPostExecute(String s) {
            if(pleaseWaitDialog.isShowing()) pleaseWaitDialog.hide();
            if (s.startsWith(getString(R.string.welcome))){
                Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                startSplashActivity(intent);
            }else{
                Toast.makeText(LoginActivity.this, getString(R.string.incorrect_username_password), Toast.LENGTH_LONG).show();
            }
        }
    }

    private class FacebookLoginAsyncTask extends AsyncTask<JsonUser, Void, String>{
        private final String TAG = WebLoginAsyncTask.class.getSimpleName();

        @Override
        protected void onPreExecute() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!pleaseWaitDialog.isShowing()) pleaseWaitDialog.show();
                }
            });
        }

        @Override
        protected String doInBackground(JsonUser... users) {
            String response;
            Gson gson = new GsonBuilder().create();
            String jsonBody = gson.toJson(users[0], JsonUser.class);
            response = JsonHelper.sendPOST(JsonHelper.HOST + "users", jsonBody);
            Log.d(TAG, response);
            if(response.startsWith("{")) {
                JsonUser jsonUser = gson.fromJson(response, JsonUser.class);
                if (!preferences.contains(JsonUser.ID_KEY)) {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(JsonUser.ID_KEY, jsonUser.id);
                    editor.apply();
                }
                if (jsonUser != null && jsonUser.status != null && jsonUser.status.equals("failed")) {
                    if (preferences.contains(JsonUser.FBID_KEY))
                        return "Facebook id exists, skipping";
                    else if (facebookUser != null) {
                        jsonBody = "{\"fbid\": \"" + facebookUser.getId() + "\"}";
                        response = JsonHelper.sendPUT(JsonHelper.HOST + "users/id/" + preferences.getString(JsonUser.ID_KEY, String.valueOf(-1)), jsonBody);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(JsonUser.FBID_KEY, facebookUser.getId());
                        editor.putString(JsonUser.NAME_KEY, facebookUser.getFirstName());
                        editor.putString(JsonUser.SURNAME_KEY, facebookUser.getLastName());
                        editor.putString(JsonUser.EMAIL_KEY, (String) facebookUser.getProperty("email"));
                        editor.apply();
                    }
                } else {
                    saveUserData(jsonUser);
                }
                Log.d(TAG, response);
            }else{
                Log.d(TAG, "Server error");
                response = null;
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(pleaseWaitDialog.isShowing()) pleaseWaitDialog.hide();
                }
            });

            if(s != null){
                Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startSplashActivity(intent);
            }else{
                Toast.makeText(LoginActivity.this, "Internal server error", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
