package com.epstim.captime.activities;

import com.epstim.captime.activities.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Toast;

import com.epstim.captime.R;
import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.dbmodel.History;
import com.epstim.captime.jsonmodel.JsonCategories;
import com.epstim.captime.jsonmodel.JsonHistory;
import com.epstim.captime.jsonmodel.JsonUser;
import com.epstim.captime.tools.DbHelper;
import com.epstim.captime.tools.JsonHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class SplashActivity extends Activity{
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        initUI();
        new PrepareAsynctask().execute();
    }

    private void initUI() {
        setContentView(R.layout.activity_splash);
        if(getActionBar() != null) getActionBar().hide();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = (int) (size.x * 0.6);
        int height = (int) (width * 0.66);
        //66%
        WebView webView = (WebView) findViewById(R.id.splashWebView);
        webView.getLayoutParams().height = height;
        webView.getLayoutParams().width = width;

        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        webView.loadUrl(getString(R.string.gif_path));
    }

    @Override
    public void onBackPressed() { }

    private class PrepareAsynctask extends AsyncTask<Void, Void, Integer>{
        private final int ERROR_CODE = -1;
        private final int OK_CODE = 1;
        private final int WRONG_CODE = 0;
        private final String TAG = PrepareAsynctask.class.getSimpleName();

        @Override
        protected Integer doInBackground(Void... params) {
            Gson gson = new GsonBuilder().create();
            try {
                //kategorie
                if (downloadCategories(gson)) return ERROR_CODE;

                //rekordy dla użytkownika
                final int id = Integer.parseInt(preferences.getString(JsonUser.ID_KEY, "-1"));
                if (id != -1) return downloadRecordsForUser(gson, id);
                else return WRONG_CODE;
            }catch(Exception ex){
                return ERROR_CODE;
            }
        }

        private Integer downloadRecordsForUser(Gson gson, int id) {
            String response;
            response = JsonHelper.sendGET(JsonHelper.HOST + "records/user_id/" + id + "/stopped/1");
            if (response != null) {
                Log.d(TAG, response);
                if (response.startsWith("{")) return OK_CODE;
                TypeToken<List<JsonHistory>> historyTypeToken = new TypeToken<List<JsonHistory>>() {
                };
                List<JsonHistory> historyItems = gson.fromJson(response, historyTypeToken.getType());
                for (JsonHistory item : historyItems) {
                    ContentValues values = new ContentValues();
                    values.put(History.C_ID, item.id);
                    values.put(History.C_CAT_ID, item.categoryId);
                    values.put(History.C_START, item.start * 1000);
                    values.put(History.C_STOP, item.stop * 1000);
                    values.put(History.C_LAT, item.latitude);
                    values.put(History.C_LONG, item.longitude);
                    getContentResolver().insert(History.URI, values);
                }
                Log.d(TAG, "Records downloaded by id");
            }
            return OK_CODE;
        }

        private boolean downloadCategories(Gson gson) {
            String response;
            response = JsonHelper.sendGET(JsonHelper.HOST + "categories");
            if (response != null) {
                Log.d(TAG, response);
                TypeToken<List<JsonCategories>> typeToken = new TypeToken<List<JsonCategories>>() {
                };
                List<JsonCategories> categoriesItems = gson.fromJson(response, typeToken.getType());
                for (JsonCategories item : categoriesItems) {
                    ContentValues values = new ContentValues();
                    values.put(Categories.C_ID, item.id);
                    values.put(Categories.C_NAME, item.name);
                    getContentResolver().insert(Categories.URI, values);
                }
            }else{
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            switch(integer){
                case ERROR_CODE:
                    Toast.makeText(SplashActivity.this, "Server error, please try again later", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    break;
                case WRONG_CODE:
                    Toast.makeText(SplashActivity.this, getString(R.string.toast_cannot_download_records_for_user), Toast.LENGTH_LONG).show();
                case OK_CODE:
                    startActivity(new Intent(getApplicationContext(), MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    break;
            }
        }
    }

}
