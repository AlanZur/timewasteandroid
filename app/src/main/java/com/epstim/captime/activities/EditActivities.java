package com.epstim.captime.activities;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.epstim.captime.R;
import com.epstim.captime.dbmodel.Categories;
import com.epstim.captime.tools.FavouritesCursorAdapter;

public class EditActivities extends Activity implements AdapterView.OnItemClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_activities);
        GridView view = (GridView) findViewById(R.id.activityEditActivitiesGridView);
        Cursor cursor = getContentResolver().query(Categories.URI, Categories.getProjection(), null, null, null);
        FavouritesCursorAdapter adapter = new FavouritesCursorAdapter(getApplicationContext(), cursor, 0);
        view.setAdapter(adapter);
        view.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Cursor cursor = ((FavouritesCursorAdapter)parent.getAdapter()).getCursor();
        cursor.moveToPosition(position);
        int is_fav = cursor.getInt(cursor.getColumnIndex(Categories.C_FAV));
        ContentValues values = new ContentValues();
        values.put(Categories.C_FAV, is_fav == 1 ? 0 : 1);
        String selection = Categories.C_ID + " = ?";
        String [] selectionArgs = {cursor.getString(cursor.getColumnIndex(Categories.C_ID))};
        getContentResolver().update(Categories.URI, values, selection, selectionArgs);
        Cursor categories = getContentResolver().query(Categories.URI, Categories.getProjection(), null, null, null);
        ((FavouritesCursorAdapter)parent.getAdapter()).changeCursor(categories);
        ((FavouritesCursorAdapter)parent.getAdapter()).notifyDataSetChanged();
    }
}
