package com.epstim.captime.FacebookOpenGraph;

import com.facebook.model.OpenGraphAction;
import com.facebook.model.PropertyName;

/**
 * Created by Alan Żur on 2014-10-22.
 */
public interface SpendAction extends OpenGraphAction{
    @PropertyName("transport")
    public void setTransport(OGObject object);
    public OGObject getTransport();

}
