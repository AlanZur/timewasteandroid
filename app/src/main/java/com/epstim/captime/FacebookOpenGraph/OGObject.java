package com.epstim.captime.FacebookOpenGraph;

import com.facebook.model.OpenGraphObject;
import com.facebook.model.PropertyName;

/**
 * Created by Alan Żur on 2014-10-22.
 */
public interface OGObject extends OpenGraphObject{
    @PropertyName("cap-time:hours")
    public void setHours(int hours);
    @PropertyName("cap-time:minutes")
    public void setMinutes(int minutes);
    @PropertyName("cap-time:seconds")
    public void setSeconds(int seconds);
    public int getHours();
    public int getMinutes();
    public int getSeconds();
    public void setUrl(String url);
    public String getUrl();
    public void setTitle(String title);
    public String getTitle();
    public void setDescription(String message);
    public String getDescription();
}
